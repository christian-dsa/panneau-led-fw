/*
 * bsp.h
 *
 *  Created on: Mar. 10, 2020
 *      Author: christian
 */

#ifndef BOARD_BSP_H_
#define BOARD_BSP_H_

#include "critical.h"
#include "led.h"
#include "light_list.h"
#include "littlefs.h"
#include "tick.h"
#include "time_util.h"

namespace bsp {

    class Board {
    public:
        enum class Error {
            none
        };

        enum class Version {
            error,
            rev100
        };

        // Board initialization
        static void init();

        // System clock
        static std::uint32_t get_system_clock_hz();

        // Time (RTC)
        static util::Time& get_time_instance();

        static void set_rtc_calibration(std::uint16_t calibration);

        static std::uint16_t get_rtc_calibration();

        // Tick
        static Tick& get_tick_instance();

        // Status Leds
        static Led& get_led_status0();
        static Led& get_led_status1();

        // Led Drivers
        static Light_list& get_light_list();

        // ADC
        static Board::Version get_board_version();

        // Modbus RTU
        static void open_rtu();
        static void close_rtu();

        // File system
        static littlefs::Block_device& get_block_device();
        static std::uint32_t get_read_size();
        static std::uint32_t get_write_size();
        static std::uint32_t get_block_size();
        static std::uint32_t get_block_count();

        // Reset
        static void reset();

        // Critical section
        /* In bsp_critical.h */

        // ISR
        static void tick_isr();
        static void modbus_rs485_isr();
        static void modbus_rtu_isr();

    private:
        Board() = default;

//        static void init_adc_read_version();
        static void init_gpio();
        static void init_tick();
        static void init_rs485();
        static void init_led_drivers();
        static void init_rtc();
        static void init_system_clock();
        static bool is_rtu_active();
        static void init_block_device();
    };
}


#endif /* BOARD_BSP_H_ */
