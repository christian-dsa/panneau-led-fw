//
// Created by christian on 2021-05-22.
//

#include "bsp.h"
#include "flash_stm32g0.h"

namespace bsp {
    using namespace stm32g0;

    // Config
    static constexpr std::uint8_t first_page = 30; // Must match value in linker file.
                                                   // Page # = ( FILE_SYSTEM_ORIGIN - FLASH_ORIGIN ) / PAGE_SIZE (0x800)
    static constexpr std::uint8_t nb_page    = 2;

    // Peripheral instance
    static Flash flash;

    // Interface implementation
    class Device_flash : public littlefs::Block_device {
    public:

        Device_flash(Flash& flash_, std::uint8_t page_start_, std::uint8_t nb_page_)
        : flash(flash_),
          start_address(flash::start_address + page_start_ * flash::page_size_byte),
          page_start(page_start_),
          nb_page(nb_page_)
        {}

        // Read a region in a block. Negative error codes are propagated
        // to the user.
        int read(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, void *buffer, lfs_size_t size) override
        {
            (void)c;

            flash::Error error = flash::Error::ok;
            auto read_iterator = (std::uint64_t*)buffer;
            std::uint32_t address = start_address + block * flash::page_size_byte + off;

            flash.unlock();
            for (std::uint32_t i = 0; i < size; i += sizeof(std::uint64_t)) {
                address += i;
                *read_iterator = flash.read<std::uint64_t>(address, error);
                ++read_iterator;
            }
            flash.lock();

            if (error != flash::Error::ok) {
                return static_cast<int>(littlefs::Error::io);
            }
            return 0;
        }

        // Program a region in a block. The block must have previously
        // been erased. Negative error codes are propagated to the user.
        // May return LFS_ERR_CORRUPT if the block should be considered bad.
        int prog(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, const void *buffer, lfs_size_t size) override
        {
            (void)c;

            flash::Error error = flash::Error::ok;
            auto write_iterator = (std::uint64_t*)buffer;
            std::uint32_t address = start_address + block * flash::page_size_byte + off;

            flash.unlock();
            for (std::uint32_t i = 0; i < size; i += sizeof(std::uint64_t)) {
                address += i;
                flash.program(address, *write_iterator, error);
                ++write_iterator;
            }
            flash.lock();

            if (error != flash::Error::ok) {
                return static_cast<int>(littlefs::Error::io);
            }
            return 0;
        }

        // Erase a block. A block must be erased before being programmed.
        // The state of an erased block is undefined. Negative error codes
        // are propagated to the user.
        // May return LFS_ERR_CORRUPT if the block should be considered bad.
        int erase(const struct lfs_config *c, lfs_block_t block) override
        {
            (void)c;

            flash::Error error = flash::Error::ok;
            std::uint8_t page = static_cast<std::uint8_t>(block) + page_start;

            flash.unlock();
            flash.page_erase(page, error);
            flash.lock();

            if (error != flash::Error::ok) {
                return static_cast<int>(littlefs::Error::io);
            }
            return 0;
        }

        // Sync the state of the underlying block device. Negative error codes
        // are propagated to the user.
        int sync(const struct lfs_config *c) override
        {
            (void)c;
            return 0;
        }

#ifdef LFS_THREADSAFE
        // Lock the underlying block device. Negative error codes
        // are propagated to the user.
        int lock(const struct lfs_config *c)
        {
            return 0;
        }

        // Unlock the underlying block device. Negative error codes
        // are propagated to the user.
        int unlock(const struct lfs_config *c)
        {
            return 0;
        }
#endif

        std::uint32_t get_nb_page() const
        {
            return nb_page;
        }

    private:
        Flash&        flash;
        std::uint32_t start_address;
        std::uint8_t  page_start;
        std::uint8_t  nb_page;
    };

    littlefs::Block_device& Board::get_block_device()
    {
        static Device_flash bd_flash(flash, first_page, nb_page);
        return bd_flash;
    }

    std::uint32_t Board::get_read_size()
    {
        return sizeof(std::uint64_t);
    }

    std::uint32_t Board::get_write_size()
    {
        return sizeof(std::uint64_t);
    }

    std::uint32_t Board::get_block_size()
    {
        return flash::page_size_byte;
    }

    std::uint32_t Board::get_block_count()
    {
        return nb_page;
    }

    void Board::init_block_device()
    {
        flash.open();
    }
}




