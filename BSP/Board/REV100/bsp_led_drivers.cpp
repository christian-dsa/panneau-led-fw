//
// Created by christian on 2020-12-06.
//

#include "bsp.h"
#include "bsp_system_clock.h"
#include "gpio_stm32g0.h"
#include "pwm_stm32g0.h"
#include "timer_util_stm32g0.h"

namespace bsp {
    using namespace stm32g0;

    static Gpio_impl gpio_pwm_ch1(gpio::Port::portA, gpio::Pin::pin8);
    static Gpio_impl gpio_pwm_ch2(gpio::Port::portA, gpio::Pin::pin9);
    static Gpio_impl gpio_pwm_ch3(gpio::Port::portC, gpio::Pin::pin6);
    static Gpio_impl gpio_pwm_ch4(gpio::Port::portA, gpio::Pin::pin10);
    static Gpio_impl gpio_pwm_ch5(gpio::Port::portA, gpio::Pin::pin11);

    static Timer_basic timer_pwm_ch1_2_4_5(TimX::tim1);
    static Timer_basic timer_pwm_ch3(TimX::tim3);

    static stm32g0::Pwm_impl pwm_ch1(timer_pwm_ch1_2_4_5, timer::Channel::ch1);
    static stm32g0::Pwm_impl pwm_ch2(timer_pwm_ch1_2_4_5, timer::Channel::ch2);
    static stm32g0::Pwm_impl pwm_ch3(timer_pwm_ch3, timer::Channel::ch1);
    static stm32g0::Pwm_impl pwm_ch4(timer_pwm_ch1_2_4_5, timer::Channel::ch3);
    static stm32g0::Pwm_impl pwm_ch5(timer_pwm_ch1_2_4_5, timer::Channel::ch4);

    static Led_dimmable led_ch1(pwm_ch1);
    static Led_dimmable led_ch2(pwm_ch2);
    static Led_dimmable led_ch3(pwm_ch3);
    static Led_dimmable led_ch4(pwm_ch4);
    static Led_dimmable led_ch5(pwm_ch5);

    static constexpr std::uint32_t nb_led = 5;
    static std::array<light_entry_t , nb_led> led_array = {{
            {led_ch1, 0},
            {led_ch2, 0},
            {led_ch3, 0},
            {led_ch4, 0},
            {led_ch5, 0}
    }};

    static constexpr std::uint32_t resolution_tick = 1000;
    static constexpr std::uint32_t freq_hz = 500;
    static constexpr std::uint32_t prescaler = stm32g0::timer::calculate_timer_prescaler<get_system_core_clock_hz(),
                                                                                         freq_hz, resolution_tick>();

    static void init_led_drivers_timer()
    {
        // Config timers
        timer_pwm_ch1_2_4_5.enable_peripheral_clk();
        timer_pwm_ch1_2_4_5.config_counting_mode(resolution_tick, prescaler);
        timer_pwm_ch1_2_4_5.start_counting_mode();

        timer_pwm_ch3.enable_peripheral_clk();
        timer_pwm_ch3.config_counting_mode(resolution_tick, prescaler);
        timer_pwm_ch3.start_counting_mode();
    }

    static void init_led_drivers_pwm()
    {
        using namespace timer;

        timer::pwm::config<TimX::tim1,
                           true, // Channel 1
                           oc::Polarity::high,
                           pwm::Mode::active,
                           true, // Channel 2
                           oc::Polarity::high,
                           pwm::Mode::active,
                           true, // Channel 3
                           oc::Polarity::high,
                           pwm::Mode::active,
                           true, // Channel 4
                           oc::Polarity::high,
                           pwm::Mode::active>();

        timer::pwm::config<TimX::tim3,
                           true,  // Channel 1
                           oc::Polarity::high,
                           pwm::Mode::active,
                           false, // Channel 2
                           oc::Polarity::high,
                           pwm::Mode::active,
                           false, // Channel 3
                           oc::Polarity::high,
                           pwm::Mode::active,
                           false, // Channel 4
                           oc::Polarity::high,
                           pwm::Mode::active>();
    }


    /* Public methods definition */
    Light_list& Board::get_light_list()
    {
        static Light_list light_list(led_array.data(), led_array.size());
        return light_list;
    }

    /* Private methods definition */
    void Board::init_led_drivers() {
        init_led_drivers_timer();
        init_led_drivers_pwm();
    }
}
