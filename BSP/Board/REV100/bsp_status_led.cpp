//
// Created by christian on 2020-12-06.
//

#include "bsp.h"
#include "gpio_stm32g0.h"

namespace bsp {
    using namespace stm32g0;

    static Gpio_impl& get_gpio_status0()
    {
        static Gpio_impl gpio_status0(gpio::Port::portB, gpio::Pin::pin9);
        return gpio_status0;
    }

    static Gpio_impl& get_gpio_status1()
    {
        static Gpio_impl gpio_status1(gpio::Port::portA, gpio::Pin::pin0);
        return gpio_status1;
    }

    /* Public methods definition */
    Led& Board::get_led_status0()
    {
        static Led led_status0(get_gpio_status0(), led::Active::high);
        return led_status0;
    }

    Led& Board::get_led_status1()
    {
        static Led led_status1(get_gpio_status1(), led::Active::high);
        return led_status1;
    }
}

