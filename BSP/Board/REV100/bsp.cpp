//
// Created by christian on 2020-10-24.
//

// Include
#include "bsp.h"
#include "os.h"

namespace bsp {
    // Need to define version
    static Board::Version version = bsp::Board::Version::error;

    /* Public methods definition */
    void Board::init()
    {
        version = bsp::Board::Version::rev100;
        init_system_clock();
        init_gpio();
        init_tick();
        init_rs485();
        init_led_drivers();
        init_rtc();
        init_block_device();
    }

    Board::Version Board::get_board_version()
    {
        return Board::Version::rev100;
    }

    // ISR
    extern "C"
    void TIM14_IRQHandler() {
        OSIntEnter();
        Board::tick_isr();
        Board::modbus_rtu_isr();
        OSIntExit();
    }

    extern "C"
    void USART2_IRQHandler() {
        OSIntEnter();
        Board::modbus_rs485_isr();
        OSIntExit();
    }
}
