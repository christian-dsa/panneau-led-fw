//
// Created by christian on 2020-12-25.
//

#include "bsp.h"
#include "bsp_system_clock.h"
#include "tick_stm32g0.h"

namespace bsp {
    using namespace stm32g0;

    /* Private functions */
    static Tick_impl& get_tick_impl()
    {
        constexpr TimX timer                       = TimX::tim14;
        constexpr IRQn_Type timer_irq              = TIM14_IRQn;
        constexpr std::uint32_t tick_priority      = 15; // Lower priority

        static Timer_basic tick_timer(timer, timer_irq, tick_priority);
        static Tick_impl tick(tick_timer);

        return tick;
    }

    /* Private methods */
    void Board::init_tick()
    {
        Tick_impl& tick = get_tick_impl();

        constexpr std::uint32_t resolution_ms = 1;
        tick.config<resolution_ms, get_system_core_clock_hz()>();

    }

    void Board::tick_isr()
    {
        get_tick_impl().interrupt_handle();
    }

    /* Public method */
    Tick& Board::get_tick_instance()
    {
        return get_tick_impl();
    }
}
