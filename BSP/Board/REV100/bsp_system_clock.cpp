//
// Created by christian on 2021-03-18.
//

#include "bsp.h"
#include "bsp_system_clock.h"
#include "rcc_stm32g0.h"
#include "stm32g0xx_ll_system.h"

namespace bsp {
    using namespace stm32g0;

    std::uint32_t Board::get_system_clock_hz()
    {
        return get_system_core_clock_hz();
    }

    void Board::init_system_clock()
    {
        // Init at 64MHz
        Rcc::enable_hsi();

        LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);

        LL_RCC_PLL_Disable();
        LL_RCC_DisableIT_PLLRDY();
        while (LL_RCC_PLL_IsReady());
        LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI, LL_RCC_PLLM_DIV_1, 8, LL_RCC_PLLR_DIV_2);
        LL_RCC_PLL_Enable();
        LL_RCC_PLL_EnableDomain_SYS();
        LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
    }
}
