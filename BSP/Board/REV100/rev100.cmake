# Target compiler options
include(${CMAKE_CURRENT_SOURCE_DIR}/Peripherals/STM32G0/stm32g0.cmake)

# Device
set(DEVICE_PN "STM32G030xx")
add_subdirectory(Peripherals/STM32G0)
target_link_libraries(board
    PUBLIC
    stm32g0_peripherals
    )

# Board
set(BOARD_INCLUDE
    ${CMAKE_CURRENT_SOURCE_DIR}/Board
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/)
set(BOARD_SOURCE
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_analog.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_fs.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_gpio.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_tick.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_reset.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_status_led.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_modbus.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_led_drivers.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_time.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_modbus.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Board/REV100/bsp_system_clock.cpp
    )
