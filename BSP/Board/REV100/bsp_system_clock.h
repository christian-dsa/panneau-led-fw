//
// Created by christian on 2021-03-12.
//

#ifndef BSP_SYSTEM_CLOCK_H
#define BSP_SYSTEM_CLOCK_H

static constexpr uint32_t get_system_core_clock_hz()
{
    return 64000000;
}

#endif //BSP_SYSTEM_CLOCK_H
