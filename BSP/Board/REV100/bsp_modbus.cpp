//
// Created by christian on 2020-12-05.
//

#include "bsp.h"
#include "bsp_system_clock.h"
#include "mb.h"
#include "uart_stm32g0.h"
#include "uart_util_stm32g0.h"

namespace bsp {
    using namespace stm32g0;

    // Need to define rtu
    static bool rtu_active = false;

    static constexpr UartX     uart_modbus = UartX::uart2;
    static constexpr IRQn_Type uart_modbus_irq = USART2_IRQn;
    static constexpr std::uint32_t uart_nvic_prio = 4;
    static constexpr std::uint32_t minimum_baudrate = 1200; // UART2 doesn't have a prescaler.

    /* Private functions */
    static Uart& get_uart()
    {
        static Uart rs485_uart(uart_modbus, Board::get_tick_instance(), uart_modbus_irq);
        return rs485_uart;
    }

    static bool is_uart_error()
    {
        Uart& uart = get_uart();
        bool error = false;

        if (uart.is_interrupt_flag_active(uart::It_flag::overrun_error) ||
            uart.is_interrupt_flag_active(uart::It_flag::parity_error)  ||
            uart.is_interrupt_flag_active(uart::It_flag::framing_error) ||
            uart.is_interrupt_flag_active(uart::It_flag::noise_error))
        {

            error = true;
        }

        return error;
    }

    static void clear_uart_error()
    {
        Uart& uart = get_uart();
        uart.clear_interrupt_flag(uart::It_flag::overrun_error);
        uart.clear_interrupt_flag(uart::It_flag::parity_error);
        uart.clear_interrupt_flag(uart::It_flag::framing_error);
        uart.clear_interrupt_flag(uart::It_flag::noise_error);
    }

    /* Public method definition */
    void Board::open_rtu()
    {
        rtu_active = true;
    }

    void Board::close_rtu()
    {
        rtu_active = false;
    }

    void Board::modbus_rs485_isr()
    {
        MB_CommRxTxISR_0_Handler();
    }

    void Board::modbus_rtu_isr()
    {
        if (Board::is_rtu_active()) {
            MB_RTU_TmrISR_Handler();
        }
    }


    /* Private method definition */
    void Board::init_rs485()
    {
        Uart& uart = get_uart();
        uart.enable_peripheral_clk();
    }

    bool Board::is_rtu_active()
    {
        return rtu_active;
    }

}

/* MODBUS Interface implementation */
using namespace bsp;
using namespace stm32g0;

void         MB_CommExit                (void)                    /* Exit       Modbus Communications                             */
{
    Uart& uart = bsp::get_uart();
    uart.disable_it(uart::It::rx_not_empty);
    uart.disable_it(uart::It::tx_cplt);
    NVIC_DisableIRQ(uart_modbus_irq);
}

void         MB_CommPortCfg             (MODBUS_CH   *pch,
                                         CPU_INT08U   port_nbr,
                                         CPU_INT32U   baud,
                                         CPU_INT08U   bits,
                                         CPU_INT08U   parity,
                                         CPU_INT08U   stops)
{
    (void)pch;
    (void)port_nbr;

    constexpr std::uint32_t clock = get_system_core_clock_hz();
    Uart& uart = bsp::get_uart();

    uart::Parity parity_;
    switch (parity) {
    case MODBUS_PARITY_NONE:
        parity_ = uart::Parity::none;
        break;
    case MODBUS_PARITY_ODD:
        parity_ = uart::Parity::odd;
        break;
    case MODBUS_PARITY_EVEN:
    default:
        parity_ = uart::Parity::even;
        break;
    }

    uart::Stop_bits stop_bits;
    switch (stops) {
    case 1:
        stop_bits = uart::Stop_bits::stop_1;
        break;
    case 2:
        stop_bits = uart::Stop_bits::stop_2;
        break;
    default:
        stop_bits = uart::Stop_bits::stop_1;
        break;
    }

    uart::Word_len word_len;
    switch (bits) {
    case 7:
        word_len = uart::Word_len::bits_8;
        break;
    case 8:
    default:
        if (parity_ == uart::Parity::none) {
            word_len = uart::Word_len::bits_8;
        } else {
            word_len = uart::Word_len::bits_9;
        }
        break;
    }

    if (baud < minimum_baudrate) {
        baud = minimum_baudrate;
    }

    uart.config(clock, baud, stop_bits, parity_, word_len);
    uart.enable_drive_enable_mode(uart::DE_polarity::high, 1, 1);

    NVIC_SetPriority(uart_modbus_irq, uart_nvic_prio);
    NVIC_EnableIRQ(uart_modbus_irq);

    uart.enable_it(uart::It::rx_not_empty);
    uart.enable_it(uart::It::error);
    uart.open();
}

void         MB_CommRxTxISR_0_Handler   (void)
{
    static Uart& uart = bsp::get_uart();
    bool error = false;

    CPU_INT08U   c;
    MODBUS_CH   *pch;

    pch = &MB_ChTbl[0];

    if (is_uart_error()) {
        clear_uart_error();
        error = true;
    }

    if (uart.is_interrupt_flag_active(uart::It_flag::rx_not_empty)) {
        uart.clear_interrupt_flag(uart::It_flag::rx_not_empty);

        c = (CPU_INT08U)uart.read_word();

        if (!error) {
            pch->RxCtr++;
            MB_RxByte(pch, c);    // Pass byte to Modbus to process
        }
    }
    if (uart.is_interrupt_flag_active(uart::It_flag::tx_cplt)) {
        uart.clear_interrupt_flag(uart::It_flag::tx_cplt);
        pch->TxCtr++;
        MB_TxByte(pch);       // Send next byte in response
    }
}

void         MB_CommRxTxISR_1_Handler   (void) {}
void         MB_CommRxTxISR_2_Handler   (void) {}
void         MB_CommRxTxISR_3_Handler   (void) {}
void         MB_CommRxTxISR_4_Handler   (void) {}
void         MB_CommRxTxISR_5_Handler   (void) {}
void         MB_CommRxTxISR_6_Handler   (void) {}
void         MB_CommRxTxISR_7_Handler   (void) {}
void         MB_CommRxTxISR_8_Handler   (void) {}
void         MB_CommRxTxISR_9_Handler   (void) {}

void         MB_CommRxIntEn             (MODBUS_CH   *pch)            /* Enable  Rx interrupts                                        */
{
    (void)pch;

    Uart& uart = bsp::get_uart();
    uart.enable_it(uart::It::rx_not_empty);
}

void         MB_CommRxIntDis            (MODBUS_CH   *pch)            /* Disable Rx interrupts                                        */
{
    (void)pch;

    Uart& uart = bsp::get_uart();
    uart.disable_it(uart::It::rx_not_empty);
}

void         MB_CommTx1                 (MODBUS_CH   *pch, CPU_INT08U   c)
{
    (void)pch;

    Uart& uart = bsp::get_uart();
    uart.transmit(&c, 1, 100);
}


void         MB_CommTxIntEn             (MODBUS_CH   *pch)            /* Enable  Tx interrupts                                        */
{
    (void)pch;

    Uart& uart = bsp::get_uart();
    uart.enable_it(uart::It::tx_cplt);
}

void         MB_CommTxIntDis            (MODBUS_CH   *pch)            /* Disable Tx interrupts                                        */
{
    (void)pch;

    Uart& uart = bsp::get_uart();
    uart.disable_it(uart::It::tx_cplt);
}

void         MB_RTU_TmrInit             (void)                        /* Initialize the timer used for RTU framing                    */
{
    Board::open_rtu();
}

void         MB_RTU_TmrExit             (void)
{
    Board::close_rtu();
}

void         MB_RTU_TmrISR_Handler      (void)
{
    MB_RTU_TmrCtr++;    // Indicate that we had activities on this interrupt
    MB_RTU_TmrUpdate(); // Check for RTU timers that have expired
}
