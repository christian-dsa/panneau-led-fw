//
// Created by christian on 2021-07-18.
//

#include "bsp.h"
#include "critical.h"
#include "bsp_system_clock.h"
#include "timer_basic_stm32g0.h"
#include "timer_util_stm32g0.h"


namespace bsp {
    using namespace stm32g0;

    static Timer_basic reset_timer(stm32g0::TimX::tim16, TIM16_IRQn, 0);
    static constexpr std::uint32_t reset_delay_ms = 1000;


    template<std::uint32_t system_clock_hz, std::uint32_t period_ms>
    void generate_timer_interrupt()
    {
        constexpr std::uint32_t freq_hz = 1;
        constexpr std::uint32_t timer_period_tick = 1000;  // Each timer tick is 1ms

        std::uint32_t prescaler = stm32g0::timer::calculate_timer_prescaler<system_clock_hz,
                                                                            freq_hz,
                                                                            timer_period_tick>();


        reset_timer.enable_peripheral_clk();
        reset_timer.config_counting_mode(period_ms, prescaler, 0);
        reset_timer.start_counting_mode();
        reset_timer.clear_if(timer::If::update); // Update IT flag is set when starting the counter
        reset_timer.enable_it(timer::It::update);
    }

    // Public method
    void Board::reset()
    {
        critical_section([](){
            generate_timer_interrupt<get_system_core_clock_hz(), reset_delay_ms>();
        });
    }

    // ISR
    extern "C"
    void TIM16_IRQHandler()
    {
        critical_section([](){
            NVIC_SystemReset();
        });
    }
}
