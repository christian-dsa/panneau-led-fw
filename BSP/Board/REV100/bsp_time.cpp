//
// Created by christian on 2020-10-27.
//

#include "bsp.h"
#include "rtc_stm32g0.h"

namespace bsp {

    using namespace stm32g0;

    class Time : public Rtc, public util::Time {

        void update_time() override
        {
            static constexpr std::uint32_t ms_factor = 1000;
            rtc::time_t rtc_time;
            rtc_time = Rtc::get_time();

            calendar.time.hour = rtc_time.hour;
            calendar.time.min = rtc_time.minute;
            calendar.time.sec = rtc_time.second;
            calendar.time.ms = static_cast<std::uint16_t>((rtc_time.subsecond * ms_factor) / rtc_time.subsecond_fraction);
        }

        void update_date() override
        {
            rtc::date_t rtc_date = Rtc::get_date();

            calendar.date.year = rtc_date.year;
            calendar.date.month = rtc_date.month;
            calendar.date.day = rtc_date.day;
            calendar.date.week_day = static_cast<std::uint8_t>(rtc_date.week_day);
        }

        void update_calendar() override
        {
            update_time();
            update_date();
        }

        void set_time(const util::time_t& time_) override
        {
            rtc::time_t time;

            time.hour = static_cast<std::uint8_t>(time_.hour);
            time.minute = time_.min;
            time.second = time_.sec;
            time.subsecond = 0;

            Rtc::set_time(time);
        }

        void set_date(const util::date_t& date_) override
        {
            rtc::date_t date {};

            date.week_day = static_cast<rtc::Week_day>(date_.week_day);
            date.day = date_.day;
            date.month = date_.month;
            date.year = date_.year;

            Rtc::set_date(date);
        }

        void set_calendar(const util::calendar_t &calendar_) override
        {
            set_time(calendar_.time);
            set_date(calendar_.date);
        }
    };


    util::Time& Board::get_time_instance()
    {
        static Time time;
        return time;
    }

    void Board::set_rtc_calibration(std::uint16_t calibration)
    {
        Rtc::set_calibration(calibration, rtc::Calib_period::_32sec);
    }

    std::uint16_t Board::get_rtc_calibration()
    {
        return Rtc::get_calibration();
    }

    /* Private methods definition */
    void Board::init_rtc() {
        Rcc::unlock_rtc_domain();
        Rcc::enable_lse_xtal(rcc::LSE_drive::low);

        Rtc::enable_peripheral_clock();
        Rtc::config(rtc::Clock_source::lse, rtc::Time_format::_24h);
    }
}
