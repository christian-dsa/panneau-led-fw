//
// Created by christian on 2021-07-18.
//

#ifndef CRITICAL_H
#define CRITICAL_H

#include <cstdint>
#include "stm32g0xx.h"

// execute must be of type: void execute()
template<class execute_t>
void critical_section(execute_t execute)
{
    std::uint32_t primask = __get_PRIMASK(); // Allow critical section to be nested

    __disable_irq();
    execute();
    __set_PRIMASK(primask); // Re-enable irq only if it was enabled at the beginning of the section
}

#endif //CRITICAL_H
