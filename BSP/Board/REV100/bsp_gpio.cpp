//
// Created by christian on 2020-12-23.
//

#include "bsp.h"
#include "gpio_stm32g0.h"
#include "gpio_port_stm32g0.h"

namespace bsp {

    using namespace stm32g0;

    void init_gpio_rev100()
    {
        gpio::enable_gpio_clock<true, true, true, false, true>();

        gpio::write_port<gpio::Port::portA, 0x0080>();
        gpio::init_port<// PortA
                        gpio::Port::portA,
                        // Pin0 - LED STATUS 1
                        gpio::Dir::output,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin1 - USART2_DE
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::high,
                        gpio::Pull::no_pull,
                        gpio::Af::af1,
                        // Pin2 - USART2_TX
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::high,
                        gpio::Pull::pull_up,
                        gpio::Af::af1,
                        // Pin3 - USART2_RX
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_up,
                        gpio::Af::af1,
                        // Pin4 - RTC_OUT_CALIBRATION   *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin5 - ADC_VIN
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin6 - ADC_CURRENT
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin7 - GPIO_NTC
                        gpio::Dir::output,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin8 - PWM_CH1
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af2,
                        // Pin9 - PWM_CH2
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af2,
                        // Pin10 - PWM_CH4
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af2,
                        // Pin11 - PWM_CH5
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af2,
                        // Pin12 - ADC_VERSION
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin13 - SWDIO
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::high,
                        gpio::Pull::pull_up,
                        gpio::Af::af0,
                        // Pin14 - SWCLK
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin15                        *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0
                        >();

        gpio::write_port<gpio::Port::portB, 0x0000>();
        gpio::init_port<// PortB
                        gpio::Port::portB,
                        // Pin0 - ADC_NTC
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin1 - ADC_BOARD_TEMP
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin2                         *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin3                         *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin4                         *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin5                         *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin6                         *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin7                         *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin8                         *** UNUSED ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin9 - LED_STATUS0
                        gpio::Dir::output,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin10                        *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin11                        *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin12                        *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin13                        *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin14                        *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0,
                        // Pin15                        *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af0
                        >();

        gpio::write_port<gpio::Port::portC, 0x00>();
        gpio::init_port<// PortC
                        gpio::Port::portC,
                        // Pin0                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin1                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin2                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin3                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin4                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin5                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin6 - PWM_CH3
                        gpio::Dir::alternate,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::pull_down,
                        gpio::Af::af1,
                        // Pin7                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin8                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin9                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin10                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin11                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin12                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin13                         *** NOT PRESENT ***
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin14 - OSC32_IN
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0,
                        // Pin15 - OSC32_OUT
                        gpio::Dir::analog,
                        gpio::Out_type::push_pull,
                        gpio::Out_speed::low,
                        gpio::Pull::no_pull,
                        gpio::Af::af0
                        >();
    }

    void Board::init_gpio()
    {
        init_gpio_rev100();
    }
}

