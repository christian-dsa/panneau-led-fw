//
// Created by christian on 2021-03-15.
//

#ifndef LIGHT_LIST_H
#define LIGHT_LIST_H

#include "led.h"

enum class Light_channel {
    all = 0,
    ch1,
    ch2,
    ch3,
    ch4,
    ch5,
    ch6,
    ch7,
    ch8
};

typedef struct light_entry {
    Led_dimmable& led;
    std::uint8_t  intensity;
} light_entry_t;

class Light_list {
public:
    Light_list(light_entry_t* light_, std::uint8_t size_)
            : light(light_),
              list_size(size_)
    {};

    enum class Error {
        ok,
        channel_out_of_range,
        do_not_support_channel_all
    };

    void open_all();
    void close_all();
    void on(Light_channel channel, std::uint8_t intensity, Error& error);
    void off(Light_channel channel, Error& error);
    bool is_on(Light_channel channel, Error& error);
    std::uint8_t get_intensity(Light_channel channel, Error& error);
    std::uint8_t size() const;

private:
    static std::uint32_t channel_to_array_index(Light_channel channel);
    void on_all(std::uint8_t intensity);
    void on_single_channel(Light_channel channel, std::uint8_t intensity);
    void off_all();
    void off_single_channel(Light_channel channel);
    bool is_channel_valid(Light_channel channel);

    light_entry_t* light;
    std::uint8_t   list_size;
};


#endif //LIGHT_LIST_H
