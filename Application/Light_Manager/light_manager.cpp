//
// Created by christian on 2021-03-14.
//

#include "light_manager.h"

void Light_manager::enter_manual_mode()
{
    mode = Mode::manual;
}

void Light_manager::enter_schedule_mode()
{
    mode = Mode::schedule;
}

Light_manager::Mode Light_manager::get_mode()
{
    return mode;
}

void Light_manager::open_light()
{
    light_list.open_all();
}

void Light_manager::close_light()
{
    light_list.close_all();
}

void Light_manager::manual_write(Light_channel channel, std::uint8_t intensity)
{
    Light_list::Error error;

    if (mode == Mode::manual) {
        light_list.on(channel, intensity, error);
    }
}

void Light_manager::schedule_write(Light_channel channel, std::uint8_t intensity)
{
    Light_list::Error error;

    if (mode == Mode::schedule) {
        light_list.on(channel, intensity, error);
    }
}

std::uint8_t Light_manager::light_list_size()
{
    return light_list.size();
}

bool Light_manager::is_on(Light_channel channel)
{
    Light_list::Error error;

    return light_list.is_on(channel, error);
}

std::uint8_t Light_manager::read(Light_channel channel)
{
    Light_list::Error error;

    return light_list.get_intensity(channel, error);
}



