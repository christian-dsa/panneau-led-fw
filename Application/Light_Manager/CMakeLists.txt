cmake_minimum_required(VERSION 3.16.9)

project(Light_Manager VERSION 0.0.0 LANGUAGES CXX C)

add_library(light_manager "")

target_compile_features(light_manager PRIVATE cxx_std_17)

set_target_properties(light_manager PROPERTIES LINKER_LANGUAGE CXX)

set(LIGHT_MANAGER_SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/light_list.cpp
    ${CMAKE_CURRENT_LIST_DIR}/light_manager.cpp
    )

set(LIGHT_MANAGER_INCLUDE
    ${CMAKE_CURRENT_LIST_DIR}
    )

target_sources(light_manager
    PRIVATE
    ${LIGHT_MANAGER_SOURCES}

    PUBLIC
    ${LIGHT_MANAGER_INCLUDE}
    )

target_include_directories(light_manager PUBLIC ${LIGHT_MANAGER_INCLUDE})

target_link_libraries(light_manager PUBLIC fw_drivers)
