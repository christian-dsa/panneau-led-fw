//
// Created by christian on 2021-03-14.
//
#include "light_list.h"

void Light_list::open_all()
{
    constexpr std::uint8_t light_off = 0;

    light_entry_t* begin = light;
    light_entry_t* end   = light + list_size;

    for (light_entry_t* it = begin; it != end; ++it) {
        it->led.open();
        it->intensity = light_off;
    }
}

void Light_list::close_all()
{
    constexpr std::uint8_t light_off = 0;

    light_entry_t* begin = light;
    light_entry_t* end   = light + list_size;

    for (light_entry_t* it = begin; it != end; ++it) {
        it->led.off();
        it->led.close();
        it->intensity = light_off;
    }
}

void Light_list::on(Light_channel channel, std::uint8_t intensity, Error& error)
{
    if (!is_channel_valid(channel)) {
        error = Error::channel_out_of_range;
        return;
    }

    if (channel == Light_channel::all) {
        on_all(intensity);
    } else {
        on_single_channel(channel, intensity);
    }

    error = Error::ok;
}

void Light_list::off(Light_channel channel, Error& error)
{
    if (!is_channel_valid(channel)) {
        error = Error::channel_out_of_range;
        return;
    }

    if (channel == Light_channel::all) {
        off_all();
    } else {
        off_single_channel(channel);
    }

    error = Error::ok;
}

bool Light_list::is_on(Light_channel channel, Error& error)
{
    return get_intensity(channel, error) > 0;
}

std::uint8_t Light_list::get_intensity(Light_channel channel, Error& error)
{
    light_entry_t* it = light + channel_to_array_index(channel);

    if (!is_channel_valid(channel)) {
        error = Error::channel_out_of_range;
        return 0;
    }

    if (channel == Light_channel::all) {
        error = Error::do_not_support_channel_all;
        return 0;
    }

    error = Error::ok;

    return it->intensity;
}

std::uint32_t Light_list::channel_to_array_index(Light_channel channel)
    {
        if (channel == Light_channel::all) {
            return 0;
        } else {
            return static_cast<std::uint32_t>(channel) - 1;
        }
    }

void Light_list::on_all(std::uint8_t intensity)
{
    light_entry_t* begin = light;
    light_entry_t* end   = light + list_size;

    for (light_entry_t* it = begin; it != end; ++it) {
        it->led.on(intensity);
        it->intensity = intensity;
    }
}

void Light_list::on_single_channel(Light_channel channel, std::uint8_t intensity)
{
    light_entry_t* it = light + channel_to_array_index(channel);

    it->led.on(intensity);
    it->intensity = intensity;
}

void Light_list::off_all()
{
    constexpr std::uint8_t light_off = 0;

    light_entry_t* begin = light;
    light_entry_t* end   = light + list_size;

    for (light_entry_t* it = begin; it != end; ++it) {
        it->led.off();
        it->intensity = light_off;
    }
}

void Light_list::off_single_channel(Light_channel channel)
{
    light_entry_t* it = light + channel_to_array_index(channel);

    it->led.off();
    it->intensity = 0;
}

bool Light_list::is_channel_valid(Light_channel channel)
{
    if (light + channel_to_array_index(channel) < (light + list_size)) {
        return true;
    } else {
        return false;
    }
}

std::uint8_t Light_list::size() const {
    return list_size;
}
