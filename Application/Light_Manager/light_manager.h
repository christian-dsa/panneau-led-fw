//
// Created by christian on 2021-03-13.
//

#ifndef LIGHT_MANAGER_H
#define LIGHT_MANAGER_H

#include "light_list.h"

class Light_manager {
public:
    enum class Mode {
        manual,
        schedule
    };

    explicit Light_manager(Light_list& light_list_)
    : light_list(light_list_),
      mode(Mode::manual)
    {}

    void enter_manual_mode();
    void enter_schedule_mode();
    Mode get_mode();

    void open_light();
    void close_light();
    void manual_write(Light_channel channel, std::uint8_t intensity);
    void schedule_write(Light_channel channel, std::uint8_t intensity);

    std::uint8_t light_list_size();
    bool is_on(Light_channel channel);
    std::uint8_t read(Light_channel channel);

private:
    Light_list& light_list;
    Mode        mode;
};


#endif //LIGHT_MANAGER_H
