/*
 * status_led.h
 *
 *  Created on: Jul. 19, 2020
 *      Author: christian
 */

#ifndef APPLICATION_STATUS_LED_H_
#define APPLICATION_STATUS_LED_H_

// Includes
#include "delay.h"
#include "led.h"

// Class
class Status_led {
public:
    enum class Status : std::uint8_t {
        startup,
        running,
        fatal_error,
    };

    Status_led(Led& green, Led& yellow, Delay& delay_)
    : led_green(green),
      led_yellow(yellow),
      delay(delay_)
    {
        led_green.set_delay_instance(delay);
        led_yellow.set_delay_instance(delay);
    }

    void show(Status_led::Status status)
    {
        switch (status) {
            case Status_led::Status::startup:
                show_startup();
                break;
            case Status_led::Status::running:
                show_running();
                break;
            case Status_led::Status::fatal_error:
                show_fatal_error();
                break;
        }
    }

private:
    void show_startup()
    {
        led_green.on();
        led_yellow.off();
    }

    void show_running()
    {
        led_yellow.off();
        led_green.flash(running_ton, running_toff);
    }

    void show_fatal_error()
    {
        led_green.off();
        led_yellow.on();
    }

    Led&   led_green;
    Led&   led_yellow;
    Delay& delay;

    static constexpr std::uint32_t running_ton  = 250;
    static constexpr std::uint32_t running_toff = 250;
};


#endif /* APPLICATION_STATUS_LED_H_ */
