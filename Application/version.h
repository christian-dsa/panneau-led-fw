//
// Created by christian on 2021-03-21.
//

#ifndef VERSION_H
#define VERSION_H

static constexpr std::uint16_t fw_version = 0x020;

#endif //VERSION_H
