/*
 * main.cpp
 *
 *  Created on: Mar. 10, 2020
 *      Author: christian
 */

// Includes
#include <os.h>
#include "memory_manager.h"
#include "startup_task.h"

int main()
{
    OS_ERR os_err;

    OSInit(&os_err);

    if (os_err != OS_ERR_NONE) {
        while (true) {
            // TODO Replace with proper error handling
        }
    }

    Memory_manager& memory_manager = get_memory_manager();
    memory_manager.init();

    Memory_manager::Block startup_task_storage = memory_manager.get_block();
    startup_task_create(startup_task_storage);

    OSStart(&os_err);

    while (true) {

    }
}

extern "C"
void __register_exitproc() {} // NOLINT(bugprone-reserved-identifier)
