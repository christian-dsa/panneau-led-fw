//
// Created by christian on 2021-05-23.
//

#include "memory_manager.h"

// Memory partition
OS_MEM mem_partition;

// Memory manager instance
static constexpr std::uint32_t nb_block   = 1;
static constexpr std::uint32_t block_size = 1024;
static std::uint8_t partition_storage[nb_block][block_size];


Memory_manager& get_memory_manager()
{
    static Memory_manager memory_manager(nb_block, block_size, &partition_storage[0][0]);
    return memory_manager;
}


// Memory_manager class
void* Memory_manager::Block::data() const
{
    return begin;
}

std::uint32_t Memory_manager::Block::size() const
{
    if (begin) {
        return block_size;
    }
    return 0;
}

void Memory_manager::Block::free()
{
    OS_ERR  err;

    OSMemPut((OS_MEM  *)manager.get_partition(),
             (void    *)begin,
             (OS_ERR  *)&err);

    begin = nullptr;
}

void Memory_manager::init()
{
    OS_ERR  err;

    OSMemCreate((OS_MEM    *)&partition,
                (CPU_CHAR  *)"Partition",
                (void      *) storage,
                (OS_MEM_QTY ) nb_block,
                (OS_MEM_SIZE) block_size,
                (OS_ERR    *)&err);
    (void) err;
}

Memory_manager::Block Memory_manager::get_block()
{
    OS_ERR  err;
    void* begin = OSMemGet(&partition, &err);
    Memory_manager::Block block(*this, begin, block_size);

    return block;
}

OS_MEM* Memory_manager::get_partition()
{
    return &partition;
}
