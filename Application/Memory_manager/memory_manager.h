//
// Created by christian on 2021-05-23.
//

#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include <cstdint>
#include "os.h"

class Memory_manager {
public:

    class Block {
    public:
        explicit Block(Memory_manager& manager_, void* begin_, std::uint32_t block_size_)
        : manager(manager_),
          begin(begin_),
          block_size(block_size_)
        {}

        void* data() const;
        std::uint32_t size() const;
        void free();

    private:
        Memory_manager& manager;
        void*           begin;
        std::uint32_t   block_size;
    };

    Memory_manager(std::uint32_t nb_block_, std::uint32_t block_size_, void* storage_)
    : partition(),
      nb_block(nb_block_),
      block_size(block_size_),
      storage(storage_)
    {}

    void init();

    Block get_block();

    OS_MEM* get_partition();

private:
    OS_MEM        partition;
    std::uint32_t nb_block;
    std::uint32_t block_size;
    void*         storage;
};

Memory_manager& get_memory_manager();

#endif //MEMORY_MANAGER_H
