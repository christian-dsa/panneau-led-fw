//
// Created by christian on 2021-08-28.
//

#include <array>
#include "bsp.h"
#include "filesystem.h"

static constexpr std::uint32_t fs_lookahead_size = 16;

static std::array<std::uint8_t, fs_cache_size> fs_prog {};
static std::array<std::uint8_t, fs_cache_size> fs_read {};
static std::array<std::uint8_t, fs_lookahead_size> fs_lookahead {};

static littlefs::fs_config_t fs_config = {
        .read_size        = sizeof(std::uint64_t),
        .prog_size        = sizeof(std::uint64_t),
        .block_size       = bsp::Board::get_block_size(),
        .block_count      = bsp::Board::get_block_count(),
        .block_cycles     = 500,
        .name_max         = 0,
        .file_max         = 0,
        .attr_max         = 0,
        .metadata_max     = 0,
        .read_buffer      = fs_read.data(),
        .prog_buffer      = fs_prog.data(),
        .cache_size       = fs_cache_size,
        .lookahead_buffer = fs_lookahead.data(),
        .lookahead_size   = fs_lookahead_size
};

Filesystem& get_filesystem()
{
    static Filesystem fs(bsp::Board::get_block_device(), fs_config);
    return fs;
}
