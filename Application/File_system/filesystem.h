//
// Created by christian on 2021-05-22.
//

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include "os.h"
#include "littlefs.h"

class Filesystem : public littlefs::File_system {
public:
    Filesystem(littlefs::Block_device& bd_, littlefs::fs_config_t& config_)
    : littlefs::File_system(bd_, config_),
      fs_mutex()
    {}

    void lock()
    {
        OS_ERR  err;
        OSMutexPend((OS_MUTEX *)&fs_mutex,
                    (OS_TICK   )0,
                    (OS_OPT    )OS_OPT_PEND_BLOCKING,
                    (CPU_TS   *)nullptr,
                    (OS_ERR   *)&err);
    }

    void unlock()
    {
        OS_ERR  err;
        OSMutexPost((OS_MUTEX *)&fs_mutex,
                    (OS_OPT    )OS_OPT_POST_NONE,
                    (OS_ERR   *)&err);
    }

private:
    OS_MUTEX fs_mutex;
};

constexpr std::uint32_t fs_cache_size = 16;

Filesystem& get_filesystem();

#endif //FILESYSTEM_H
