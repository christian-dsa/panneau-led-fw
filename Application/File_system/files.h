//
// Created by christian on 2021-09-09.
//

#ifndef FILES_H
#define FILES_H

#include "littlefs.h"

// Files
constexpr const char*   file_baud_rate = "baud_rate";
constexpr const char*   file_address   = "address";
constexpr const char*   file_parity    = "parity";
constexpr const char*   file_stop_bit  = "stop_bit";
constexpr const char*   file_rtc_calib = "rtc_calib";
constexpr std::int32_t  retry_max      = 1;

template<typename T>
static void init_file_value(littlefs::File* file, const char* file_name, T& field, T default_value)
{
    littlefs::Error file_error;

    file->open(file_name, "r", file_error);
    if (file_error == littlefs::Error::no_entry) {
        field = default_value;
        file->open(file_name, "w");
        file->write(field);
        file->close();
    } else {
        file->read(field);
        file->close();
    }
}

// Return 1 if write is a success.
template<typename T>
static bool write_file(littlefs::File* file, const char* file_name, T& value, std::int32_t retry)
{
    std::uint16_t r_value;
    do {
        // Write file
        file->open(file_name, "w");
        file->write(value);
        file->close();

        // Verify
        file->open(file_name, "r");
        file->read(r_value);
        file->close();

        --retry;
    } while ((r_value != value) && (retry >= 0));

    return r_value == value;
}

#endif //FILES_H
