//
// Created by christian on 2020-12-04.
//

#ifndef ANALOG_MEASUREMENTS_H
#define ANALOG_MEASUREMENTS_H

// Includes
#include <cstdint>

class Analog {
public:

    virtual void config(std::uint32_t sampling_rate_ms) = 0;

    virtual void start() = 0;

    virtual void stop() = 0;

    virtual void get_input_current_ma() = 0;

    virtual void get_input_voltage_mv() = 0;

    virtual void get_ext_temperature_voltage_uv() = 0;

    virtual void get_board_temperature_voltage_uv() = 0;

    virtual std::uint8_t get_board_version() = 0;

    virtual ~Analog() = default;
};


#endif //ANALOG_MEASUREMENTS_H
