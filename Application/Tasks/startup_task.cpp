//
// Created by christian on 2021-03-16.
//
#include <array>
#include "app_delay.h"
#include "bsp.h"
#include "idle_task.h"
#include "files.h"
#include "filesystem.h"
#include "light_task.h"
#include "memory_manager.h"
#include "mb_data.h"
#include "modbus_task.h"
#include "status_led.h"
#include "startup_task.h"
#include "tasks_config.h"

// Startup Task
static OS_TCB  startup_task_tcb;

// Task
static void startup_task(void* arg);

// Public functions
void startup_task_create(Memory_manager::Block& block)
{
    OS_ERR os_err;

    OSTaskCreate((OS_TCB*)      &startup_task_tcb,
                 (CPU_CHAR*)    "Startup task",
                 (OS_TASK_PTR)  startup_task,
                 (void*)        nullptr,
                 (OS_PRIO)      startup_task_priority,
                 (CPU_STK*)     block.data(),
                 (CPU_STK_SIZE) (block.size() / sizeof(CPU_STK)) / 10,
                 (CPU_STK_SIZE) block.size() / sizeof(CPU_STK),
                 (OS_MSG_QTY)   0,
                 (OS_TICK)      0,
                 (void*)        &block,
                 (OS_OPT)       (OS_OPT_TASK_NO_TLS | OS_OPT_TASK_STK_CLR | OS_OPT_TASK_STK_CHK),
                 (OS_ERR*)      &os_err);
}

// Private functions
void startup_task(void* arg)
{
    (void)arg;
    OS_ERR os_err;

    // Board
    bsp::Board::init();
    Tick& tick = bsp::Board::get_tick_instance();
    tick.start();

    // Systick
    CPU_Init();
    OS_CPU_SysTickInitFreq(bsp::Board::get_system_clock_hz());

    // Status led
    Delay_blocking delay_blocking(tick);
    Led& led_status_yellow = bsp::Board::get_led_status0();
    Led& led_status_green  = bsp::Board::get_led_status1();

    Status_led status_led(led_status_green, led_status_yellow, delay_blocking);
    status_led.show(Status_led::Status::startup);

    // File system
    littlefs::Error fs_error;
    Filesystem& fs = get_filesystem();

    fs.lock();
    fs.mount(fs_error);

    if (fs_error != littlefs::Error::ok) {
        fs.format(fs_error);
        fs.mount(fs_error);
    }

    // BSP config file
    static std::array<std::uint8_t, fs_cache_size> bsp_file_cache {};
    littlefs::File bsp_file(fs.get_lfs(), bsp_file_cache.data());

    // RTC Calibration
    constexpr std::uint16_t default_rtc_calibration_value = 0;
    std::uint16_t calibration_value;
    init_file_value(&bsp_file, file_rtc_calib, calibration_value, default_rtc_calibration_value);

    fs.unlock();

    // Light task
    light_task_create();

    // Idle task
    idle_task_create(&startup_task_tcb);

    // Initialization completed, delete itself
    OSTaskDel(nullptr, &os_err);
}
