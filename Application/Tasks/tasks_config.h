//
// Created by christian on 2021-03-15.
//

#ifndef TASKS_CONFIG_H
#define TASKS_CONFIG_H

#include <cstdint>
#include "os_cfg.h"

static constexpr std::uint8_t  light_task_priority = OS_CFG_PRIO_MAX - 3;
static constexpr std::uint32_t light_task_size     = 128;

// Startup task
static constexpr std::uint8_t  startup_task_priority = OS_CFG_PRIO_MAX - 4;
static constexpr std::uint32_t startup_task_size     = 128;

// Idle task
static constexpr std::uint8_t  idle_task_priority = OS_CFG_PRIO_MAX - 2;  // Lowest Priority
static constexpr std::uint32_t idle_task_size     = 64;

// Modbus config in Application/Modbus/mb_cfg.h


#endif //TASKS_CONFIG_H
