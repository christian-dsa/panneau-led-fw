//
// Created by christian on 2021-03-16.
//

#ifndef STARTUP_TASK_H
#define STARTUP_TASK_H

void startup_task_create(Memory_manager::Block& block);

#endif //STARTUP_TASK_H
