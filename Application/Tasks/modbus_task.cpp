//
// Created by christian on 2021-03-17.
//

#include <cstdint>
#include "bsp.h"
#include "filesystem.h"
#include "mb.h"
#include "mb_data.h"
#include "modbus_task.h"

constexpr std::uint32_t khz_to_hz = 1000;

/* Modbus task taken from Library/Micrium/uC-MODBUS/OS/uCOS-III/mb_os.c */
static  OS_TCB     MB_OS_RxTaskTCB;
static  CPU_STK    MB_OS_RxTaskStk[MB_OS_CFG_RX_TASK_STK_SIZE];

static  void  MB_OS_InitSlave ();
static  void  MB_OS_ExitSlave ();
static  void  MB_OS_RxTask    (void  *p_arg);

void modbus_task_create()
{
    std::uint32_t rtu_timer_freq = 1/bsp::Board::get_tick_instance().get_resolution_ms() * khz_to_hz;

    MB_Init(rtu_timer_freq);
}

void  MB_OS_Init (void)
{
    MB_OS_InitSlave();
}

static  void  MB_OS_InitSlave ()
{
    OS_ERR  err;


    OSTaskCreate(&MB_OS_RxTaskTCB,
                 (CPU_CHAR   *)"Modbus Rx Task",
                 MB_OS_RxTask,
                 (void       *)nullptr,
                 MB_OS_CFG_RX_TASK_PRIO,
                 &MB_OS_RxTaskStk[0],
                 MB_OS_CFG_RX_TASK_STK_SIZE / 10,
                 MB_OS_CFG_RX_TASK_STK_SIZE,
                 MODBUS_CFG_MAX_CH,
                 0,
                 (void      *)nullptr,
                 (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 &err);
}

void  MB_OS_Exit ()
{
    MB_OS_ExitSlave();
}

void  MB_OS_ExitSlave ()
{
    OS_ERR  err;

    OSTaskDel(&MB_OS_RxTaskTCB,                               /* Delete Modbus Rx Task                 */
              &err);
    (void)err;
}

void  MB_OS_RxSignal (MODBUS_CH *pch)
{
    OS_ERR  err;


    if (pch != (MODBUS_CH *)nullptr) {
        switch (pch->MasterSlave) {

            case MODBUS_SLAVE:
                default:
                    (void)OSTaskQPost(&MB_OS_RxTaskTCB,
                                      pch,
                                      sizeof(void *),
                                      OS_OPT_POST_FIFO,
                                      &err);
                    break;
        }
    }
}

void  MB_OS_RxWait (MODBUS_CH   *pch, CPU_INT16U  *perr)
{
    (void)pch;
    *perr = MODBUS_ERR_INVALID;
}

static  void  MB_OS_RxTask (void *p_arg)
{
    OS_ERR       err;
    OS_MSG_SIZE  msg_size;
    CPU_TS       ts;
    MODBUS_CH   *pch;

    (void)p_arg;

    // Set modbus config files
    static std::array<std::uint8_t, fs_cache_size> mb_file_cache {};

    Filesystem& fs = get_filesystem();
    littlefs::File mb_file(fs.get_lfs(), mb_file_cache.data());
    get_modbus_instance().set_file(mb_file);

    while (!get_modbus_instance().is_init_complete()) {
        // TODO Add error handling
    }

    // Serial config init
    Modbus::Serial_config config = get_modbus_instance().get_serial_config();

    MB_CfgCh(config.address,                     // ... Modbus Node # for this slave channel
             MODBUS_SLAVE,                          // ... This is a SLAVE
             0,                                  // ... 0 when a slave
             MODBUS_MODE_RTU,                       // ... Modbus Mode (_ASCII or _RTU)
             2,                                  // ... Specify UART #1    ** IMPORTANT: Set in BSP, not here **
             config.get_baudrate_numeric_value(),   // ... Baud Rate
             8,                                  // ... Number of data bits 7 or 8.
             config.parity,                         // ... Parity: _NONE, _ODD or _EVEN
             config.stop_bit,                    // ... Number of stop bits 1 or 2.
             MODBUS_WR_EN);                         // ... Enable (_EN) or disable (_DIS) writes

    // Task
    while (DEF_TRUE) {
        pch = (MODBUS_CH *)OSTaskQPend(0,        /* Wait for a packet to be received                   */
                                       OS_OPT_PEND_BLOCKING,
                                       &msg_size,
                                       &ts,
                                       &err);
        MB_RxTask(pch);                          /* Process the packet received                        */
    }
}
