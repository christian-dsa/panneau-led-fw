//
// Created by christian on 2021-03-15.
//

#ifndef APP_DELAY_H
#define APP_DELAY_H

#include "delay.h"
#include "os.h"
#include "timer.h"

class Delay_os : public Delay {
public:
    void delay_ms(std::uint32_t ms) override
    {
        OS_ERR os_err;

        OSTimeDlyHMSM(0, 0, 0, ms, OS_OPT_TIME_HMSM_NON_STRICT, &os_err);
    }

    void delay_sec(std::uint32_t sec) override
    {
        OS_ERR os_err;
        auto sec_ = static_cast<std::uint16_t>(sec & 0xFFFF);

        OSTimeDlyHMSM(0, 0, sec_, 0, OS_OPT_TIME_HMSM_NON_STRICT, &os_err);
    }
};

class Delay_blocking : public Delay {
public:
    Delay_blocking(Tick& tick_)
    : timer(tick_)
    {}

    void delay_ms(std::uint32_t ms) override
    {
        timer.delay_ms(ms);
    }

    void delay_sec(std::uint32_t sec) override
    {
        timer.delay_ms(sec * 1000);
    }

    Timer timer;
};

#endif //APP_DELAY_H
