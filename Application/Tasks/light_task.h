//
// Created by christian on 2021-03-15.
//

#ifndef LIGHT_TASK_H
#define LIGHT_TASK_H

#include "light_manager.h"

void light_task_create();

Light_manager& light_task_get_light_manager();

#endif //LIGHT_TASK_H
