//
// Created by christian on 2021-05-24.
//
#include "bsp.h"
#include "app_delay.h"
#include "memory_manager.h"
#include "status_led.h"
#include "tasks_config.h"

// Idle Task
static OS_TCB  idle_task_tcb;
static CPU_STK idle_task_stk[idle_task_size];

static void idle_task(void* arg);

// Public functions
void idle_task_create(OS_TCB* startup_tcb)
{
    OS_ERR os_err;

    OSTaskCreate((OS_TCB*)      &idle_task_tcb,
                 (CPU_CHAR*)    "Idle task",
                 (OS_TASK_PTR)  idle_task,
                 (void*)        startup_tcb,
                 (OS_PRIO)      idle_task_priority,
                 (CPU_STK*)     &idle_task_stk[0],
                 (CPU_STK_SIZE) idle_task_size / 10,
                 (CPU_STK_SIZE) idle_task_size,
                 (OS_MSG_QTY)   0,
                 (OS_TICK)      0,
                 (void*)        nullptr,
                 (OS_OPT)       (OS_OPT_TASK_NO_TLS | OS_OPT_TASK_STK_CLR | OS_OPT_TASK_STK_CHK),
                 (OS_ERR*)      &os_err);
}

// Private functions
void idle_task(void* arg)
{
    // Free startup task memory block
    auto tcb = reinterpret_cast<OS_TCB*>(arg);
    auto block = reinterpret_cast<Memory_manager::Block*>(tcb->ExtPtr);

    // Status led
    Tick& tick = bsp::Board::get_tick_instance();
    Delay_blocking delay_blocking(tick);
    Led& led_status_yellow = bsp::Board::get_led_status0();
    Led& led_status_green  = bsp::Board::get_led_status1();

    Status_led status_led(led_status_green, led_status_yellow, delay_blocking);
    status_led.show(Status_led::Status::startup);

    if (tcb->TaskState == OS_TASK_STATE_DEL) {
        block->free();
    } else {
        // Error
        status_led.show(Status_led::Status::fatal_error);
        while (true);
    }

    while (true) {
        status_led.show(Status_led::Status::running);
    }
}
