//
// Created by christian on 2021-05-24.
//

#ifndef IDLE_TASK_H
#define IDLE_TASK_H

// Free startup task's memory block
void idle_task_create(OS_TCB* startup_tcb);

#endif //IDLE_TASK_H
