//
// Created by christian on 2021-03-15.
//

#include "app_delay.h"
#include "bsp.h"
#include "light_task.h"
#include "mb_data.h"
#include "modbus_task.h"
#include "tasks_config.h"

// OS
static OS_TCB  light_task_tcb;
static CPU_STK light_task_stk[light_task_size];

// Task
static void light_task(void* arg);

// Public function
void light_task_create()
{
    OS_ERR os_err;

    OSTaskCreate((OS_TCB*)      &light_task_tcb,
                 (CPU_CHAR*)    "Light task",
                 (OS_TASK_PTR)  light_task,
                 (void*)        nullptr,
                 (OS_PRIO)      light_task_priority,
                 (CPU_STK*)     &light_task_stk[0],
                 (CPU_STK_SIZE) light_task_size / 10,
                 (CPU_STK_SIZE) light_task_size,
                 (OS_MSG_QTY)   0,
                 (OS_TICK)      0,
                 (void*)        nullptr,
                 (OS_OPT)       (OS_OPT_TASK_NO_TLS | OS_OPT_TASK_STK_CLR | OS_OPT_TASK_STK_CHK),
                 (OS_ERR*)      &os_err);
}

// Private function
static void light_task(void* arg)
{
    (void) arg;

    // Delay
    Delay_os delay_os;

    // Light manager
    Light_manager light_manager(bsp::Board::get_light_list());
    light_manager.open_light();

    // Modbus
    get_modbus_instance().set_light_manager(light_manager);
    modbus_task_create();

    while (true) {
        delay_os.delay_sec(1);
    }
}
