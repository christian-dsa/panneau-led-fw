
# Executable output
set(EXECUTABLE ${PROJECT_NAME}.elf)

add_executable(${EXECUTABLE} "")

target_compile_features(${EXECUTABLE} PRIVATE cxx_std_17)
target_compile_features(${EXECUTABLE} PRIVATE c_std_11)

set_target_properties(${EXECUTABLE} PROPERTIES LINKER_LANGUAGE CXX)

### if BOARD == REV100 ###
# Startup
set(STARTUP_PATH "${CMAKE_CURRENT_LIST_DIR}/Startup")
set(STARTUP_SOURCE
    ${STARTUP_PATH}/gcc/startup_stm32g030xx.S
    )
# Linker
set(LINKER_SCRIPT "${CMAKE_CURRENT_LIST_DIR}/Linker/STM32G030K8TX_FLASH.ld"
    )

### END IF REV100 ###

# Application
set(APPLICATION_SOURCES
    ${STARTUP_SOURCE}
    ${CMAKE_CURRENT_LIST_DIR}/main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/File_system/filesystem.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Memory_manager/memory_manager.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Tasks/idle_task.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Tasks/light_task.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Tasks/startup_task.cpp
    ${CMAKE_CURRENT_LIST_DIR}/Tasks/modbus_task.cpp
    )

set(APPLICATION_INCLUDE
    ${CMAKE_CURRENT_LIST_DIR}/File_system
    ${CMAKE_CURRENT_LIST_DIR}/Memory_manager
    ${CMAKE_CURRENT_LIST_DIR}/Status
    ${CMAKE_CURRENT_LIST_DIR}/Tasks
    ${CMAKE_CURRENT_LIST_DIR}
    )

set(APPLICATION_DEFINES ${APPLICATION_DEFINES}
    UCOS
    )

target_sources(${EXECUTABLE} PRIVATE ${APPLICATION_SOURCES})

target_include_directories(${EXECUTABLE} PRIVATE ${APPLICATION_INCLUDE})

target_compile_definitions(${EXECUTABLE} PUBLIC ${APPLICATION_DEFINES})

target_link_libraries(${EXECUTABLE} PRIVATE board light_manager modbus rtos)

# Linker
target_link_options(${EXECUTABLE} PRIVATE
    -T ${LINKER_SCRIPT}
    -Wl,-Map=${PROJECT_NAME}.map
    )

# Print executable size
add_custom_command(TARGET ${EXECUTABLE}
    POST_BUILD
    COMMAND arm-none-eabi-size ${EXECUTABLE}
    )

# Create hex file
add_custom_command(TARGET ${EXECUTABLE}
    POST_BUILD
    COMMAND arm-none-eabi-objcopy -O ihex ${EXECUTABLE} ${PROJECT_NAME}.hex
    COMMAND arm-none-eabi-objcopy -O binary ${EXECUTABLE} ${PROJECT_NAME}.bin
    )
