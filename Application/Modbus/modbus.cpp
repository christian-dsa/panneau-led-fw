//
// Created by christian on 2021-03-14.
//
#include "bsp.h"
#include "files.h"
#include "modbus.h"
#include "version.h"

// Class methods
bool Modbus::coil_read(std::uint16_t coil, Modbus::Error& error)
{
    return discrete_input_read(coil, error);
}

void Modbus::coil_write(std::uint16_t coil, bool value, Modbus::Error& error)
{
    error = Modbus::Error::out_of_range;
    (void)coil;
    (void)value;
}

bool Modbus::discrete_input_read(std::uint16_t input, Modbus::Error& error)
{
    error = Modbus::Error::ok;

    if (is_light_status_discrete_input(input)) {
        Light_channel channel = convert_discrete_input_to_channel(input);

        return light_manager->is_on(channel);
    }

    error = Modbus::Error::out_of_range;
    return false;
}

std::uint16_t Modbus::input_register_read(std::uint16_t reg, Modbus::Error& error)
{
    // Map all holding registers to input registers
    return holding_register_read(reg, error);
}

std::uint16_t Modbus::holding_register_read(std::uint16_t reg, Modbus::Error& error)
{
    error = Modbus::Error::ok;
    std::uint16_t value;

    if (is_light_intensity_register(reg)) {
        Light_channel channel = convert_register_to_channel(reg);
        return light_manager->read(channel);
    }

    switch (reg) {
    case reg_light_mode_address:
        value = reg_read_light_mode();
        break;
    case reg_fw_version_address:
        value = fw_version;
        break;
    case reg_board_version_address:
        value = reg_read_board_version();
        break;
    case reg_serial_address:
        value = current_config.address;
        break;
    case reg_serial_baud_rate_address:
        value = current_config.baud_rate;
        break;
    case reg_serial_parity_address:
        value = current_config.parity;
        break;
    case reg_serial_stop_bit_address:
        value = current_config.stop_bit;
        break;
    case reg_rtc_second:
        value = reg_read_rtc_second();
        break;
    case reg_rtc_minute:
        value = reg_read_rtc_minute();
        break;
    case reg_rtc_hour:
        value = reg_read_rtc_hour();
        break;
    case reg_rtc_week_day:
        value = reg_read_rtc_week_day();
        break;
    case reg_rtc_calibration:
        value = bsp::Board::get_rtc_calibration();
        break;
    default:
        value = 0;
        error = Modbus::Error::out_of_range;
        break;
    }

    return value;
}

void Modbus::holding_register_write(std::uint16_t reg, std::uint16_t value, Modbus::Error& error)
{
    error = Modbus::Error::ok;

    if (is_light_intensity_register(reg)) {
        reg_write_light_intensity(reg, value, error);
        return;
    }

    switch (reg) {
    case reg_light_mode_address:
        reg_write_light_mode(value, error);
        break;
    case reg_serial_address:
        reg_write_serial_address(value, error);
        break;
    case reg_serial_baud_rate_address:
        reg_write_serial_baudrate(value, error);
        break;
    case reg_serial_parity_address:
        reg_write_serial_parity(value, error);
        break;
    case reg_serial_stop_bit_address:
        reg_write_serial_stop_bit(value, error);
        break;
    case reg_reset_address:
        reg_write_reset(value, error);
        break;
    case reg_rtc_second:
        reg_write_rtc_sec(value, error);
        break;
    case reg_rtc_minute:
        reg_write_rtc_min(value, error);
        break;
    case reg_rtc_hour:
        reg_write_rtc_hr(value, error);
        break;
    case reg_rtc_week_day:
        reg_write_rtc_w_day(value, error);
        break;
    case reg_rtc_calibration:
        reg_write_rtc_calibration(value, error);
        break;
    default:
        error = Modbus::Error::out_of_range;
        break;
    }
}

void Modbus::set_light_manager(Light_manager& light_manager_)
{
    light_manager = &light_manager_;
}

void Modbus::set_file(littlefs::File& file_)
{
    file = &file_;
}

bool Modbus::is_init_complete()
{
    if ((file != nullptr) && (light_manager != nullptr)) {
        return true;
    }
    return false;
}

Modbus::Serial_config& Modbus::get_serial_config()
{
    init_baud_rate();
    init_address();
    init_parity();
    init_stop_bit();

    return current_config;
}


/// Private
bool Modbus::is_light_intensity_register(std::uint16_t reg)
{
    if (!light_manager) {
        // no light manager
        return false;
    }

    static constexpr std::uint16_t end = 1;

    std::uint16_t reg_start = reg_light_channel_intensity_address;
    std::uint16_t reg_end = reg_light_channel_intensity_address + light_manager->light_list_size() + end;

    return ((reg >= reg_start) && (reg < reg_end));
}

bool Modbus::is_light_status_discrete_input(std::uint16_t input)
{
    if (!light_manager) {
        // no light manager
        return false;
    }

    static constexpr std::uint16_t end = 1;

    std::uint16_t reg_start = discrete_input_light_channel_status_start;
    std::uint16_t reg_end = discrete_input_light_channel_status_start + light_manager->light_list_size() + end;

    // Channel all is not supported
    return ((input > reg_start) && (input <= reg_end));
}

Light_channel Modbus::convert_register_to_channel(std::uint16_t reg)
{
    return static_cast<Light_channel>(reg - reg_light_channel_intensity_address);
}

Light_channel Modbus::convert_discrete_input_to_channel(std::uint16_t input)
{
    return static_cast<Light_channel>(input - discrete_input_light_channel_status_start);
}

void Modbus::reg_write_light_intensity(std::uint16_t reg, std::uint16_t value, Error& error)
{
    auto intensity = static_cast<std::uint8_t>(value);

    if (intensity > reg_light_channel_intensity_max) {
        intensity = reg_light_channel_intensity_max;
        error = Error::write_error;
    }

    Light_channel channel = convert_register_to_channel(reg);
    light_manager->manual_write(channel, intensity);
}

void Modbus::reg_write_light_mode(std::uint16_t value, Error& error)
{
    switch (value) {
    case reg_light_mode_manual:
        light_manager->enter_manual_mode();
        break;
    case reg_light_mode_schedule:
        light_manager->enter_schedule_mode();
        break;
    default:
        error = Modbus::Error::write_error;
        break;
    }
}

void Modbus::reg_write_reset(std::uint16_t value, Error& error)
{
    error = Modbus::Error::write_error;

    if (value == reg_reset_key) {
        error = Modbus::Error::ok;
        bsp::Board::reset();
    }
}

void Modbus::reg_write_serial_baudrate(std::uint16_t value, Error& error)
{

    if (value > Serial_config::Baud_rate::_115200_baud) {
        error = Modbus::Error::write_error;
        return;
    }

    if (!write_file(file, file_baud_rate, value, retry_max)) {
        error = Modbus::Error::write_error;
    }
}

void Modbus::reg_write_serial_address(std::uint16_t value, Error& error)
{
    if ((value > Serial_config::default_serial_address) || (value == Serial_config::broadcast_address)) {
        error = Modbus::Error::write_error;
        return;
    }

    if (!write_file(file, file_address, value, retry_max)) {
        error = Modbus::Error::write_error;
    }
}

void Modbus::reg_write_serial_parity(std::uint16_t value, Error& error)
{
    if (value > Serial_config::Parity::even) {
        error = Modbus::Error::write_error;
        return;
    }

    if (!write_file(file, file_parity, value, retry_max)) {
        error = Modbus::Error::write_error;
    }
}

void Modbus::reg_write_serial_stop_bit(std::uint16_t value, Error& error)
{
    if ((value > Serial_config::Stop_bit::_2) || (value < Serial_config::Stop_bit::_1)) {
        error = Modbus::Error::write_error;
        return;
    }

    if (!write_file(file, file_stop_bit, value, retry_max)) {
        error = Modbus::Error::write_error;
    }
}

void Modbus::reg_write_rtc_sec(std::uint16_t value, Error& error)
{
    constexpr std::uint8_t max = 59;

    util::Time& instance = bsp::Board::get_time_instance();
    util::time::time_t t = instance.get_time();

    auto input = static_cast<std::uint8_t>(value);
    if ( input <= max) {
        t.sec = input;
    } else {
        error = Modbus::Error::write_error;
    }

    instance.set_time(t);
}

void Modbus::reg_write_rtc_min(std::uint16_t value, Error& error)
{
    constexpr std::uint8_t max = 59;

    util::Time& instance = bsp::Board::get_time_instance();
    util::time::time_t t = instance.get_time();

    auto input = static_cast<std::uint8_t>(value);
    if ( input <= max) {
        t.min = input;
    } else {
        error = Modbus::Error::write_error;
    }

    instance.set_time(t);
}

void Modbus::reg_write_rtc_hr(std::uint16_t value, Error& error)
{
    constexpr std::uint8_t max = 23;

    util::Time& instance = bsp::Board::get_time_instance();
    util::time::time_t t = instance.get_time();

    if ( value <= max) {
        t.hour = value;
    } else {
        error = Modbus::Error::write_error;
    }

    instance.set_time(t);
}

void Modbus::reg_write_rtc_w_day(std::uint16_t value, Error& error)
{
    constexpr auto max = util::time::week_day::sunday;

    util::Time& instance = bsp::Board::get_time_instance();
    util::time::date_t date = instance.get_date();

    if ( value <= max) {
        date.week_day = value;
    } else {
        error = Modbus::Error::write_error;
    }

    instance.set_date(date);
}

void Modbus::reg_write_rtc_calibration(std::uint16_t value, Error& error)
{
    bsp::Board::set_rtc_calibration(value);

    if (!write_file(file, file_rtc_calib, value, retry_max)) {
        error = Modbus::Error::write_error;
    }
}

std::uint16_t Modbus::reg_read_light_mode()
{
    std::uint16_t mode;
    if (light_manager->get_mode() == Light_manager::Mode::manual) {
        mode = reg_light_mode_manual;
    } else {
        mode = reg_light_mode_schedule;
    }
    return mode;
}

std::uint16_t Modbus::reg_read_board_version()
{
    if (bsp::Board::get_board_version() == bsp::Board::Version::rev100) {
        return 0x100;
    }
    return 0;
}

std::uint16_t Modbus::reg_read_rtc_second()
{
    util::Time& instance = bsp::Board::get_time_instance();

    return instance.get_time().sec;
}

std::uint16_t Modbus::reg_read_rtc_minute()
{
    util::Time& instance = bsp::Board::get_time_instance();
    return instance.get_time().min;
}

std::uint16_t Modbus::reg_read_rtc_hour()
{
    util::Time& instance = bsp::Board::get_time_instance();
    return instance.get_time().hour;
}

std::uint16_t Modbus::reg_read_rtc_week_day()
{
    util::Time& instance = bsp::Board::get_time_instance();
    return instance.get_date().week_day;
}

void Modbus::init_baud_rate()
{
    init_file_value(file, file_baud_rate, current_config.baud_rate, Serial_config::default_serial_baud_rate);
}

void Modbus::init_address()
{
    init_file_value(file, file_address, current_config.address, Serial_config::default_serial_address);
}

void Modbus::init_parity()
{
    init_file_value(file, file_parity, current_config.parity, Serial_config::default_serial_parity);
}

void Modbus::init_stop_bit()
{
    init_file_value(file, file_stop_bit, current_config.stop_bit, Serial_config::default_serial_stop_bit);
}
