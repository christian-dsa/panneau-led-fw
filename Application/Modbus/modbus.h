//
// Created by christian on 2021-03-14.
//

#ifndef MODBUS_H
#define MODBUS_H

#include <array>
#include <cstdint>
#include "light_manager.h"
#include "littlefs.h"

class Modbus {
public:
    Modbus()
    : light_manager(nullptr),
      file(nullptr),
      current_config()
    {}

    enum class Error {
        ok,
        out_of_range,
        write_error
    };

    struct Serial_config {
        enum Parity : std::uint8_t { // Fit Micrium MODBUS_PARITY _NONE _ODD _EVEN. If another stack is used, make translator.
            none = 0,
            odd  = 1,
            even = 2
        };

        enum Baud_rate : std::uint8_t { // Fit Micrium MODBUS_PARITY _NONE _ODD _EVEN. If another stack is used, make translator.
            _300_baud    = 0,
            _600_baud    = 1,
            _1200_baud   = 2,
            _2400_baud   = 3,
            _4800_baud   = 4,
            _9600_baud   = 5,
            _19200_baud  = 6,
            _38400_baud  = 7,
            _57600_baud  = 8,
            _115200_baud = 9
        };

        enum Stop_bit : std::uint8_t {
            _1 = 1,
            _2 = 2
        };

        std::uint32_t get_baudrate_numeric_value() const
        {
            return baudrate_lt[baud_rate];
        }

        Baud_rate    baud_rate;
        Parity       parity;
        Stop_bit     stop_bit;
        std::uint8_t address;

        // Default values
        static constexpr Baud_rate     default_serial_baud_rate = Baud_rate::_19200_baud;
        static constexpr Parity        default_serial_parity    = Parity::even;
        static constexpr Stop_bit      default_serial_stop_bit  = Stop_bit::_1;
        static constexpr std::uint8_t  default_serial_address   = 247;
        static constexpr std::uint8_t  broadcast_address        = 0;

    private:
        static constexpr std::array<std::uint32_t, 10> baudrate_lt = {300 , 600  , 1200 , 2400 , 4800,
                                                                      9600, 19200, 38400, 57600, 115200};
    };

    // Registers
    bool coil_read(std::uint16_t coil, Error& error);
    void coil_write(std::uint16_t coil, bool value, Error& error);
    bool discrete_input_read(std::uint16_t input, Error& error);
    std::uint16_t input_register_read(std::uint16_t reg, Error& error);
    std::uint16_t holding_register_read(std::uint16_t reg, Error& error);
    void holding_register_write(std::uint16_t reg, std::uint16_t value, Error& error);

    // Objects
    void set_light_manager(Light_manager& light_manager_);
    void set_file(littlefs::File& file);
    bool is_init_complete();

    // Serial config
    Serial_config& get_serial_config();

private:
    // Register address
    bool is_light_intensity_register(std::uint16_t reg);
    bool is_light_status_discrete_input(std::uint16_t input);
    static Light_channel convert_register_to_channel(std::uint16_t reg);
    static Light_channel convert_discrete_input_to_channel(std::uint16_t input);

    // Register write
    void reg_write_light_intensity(std::uint16_t reg, std::uint16_t value, Error& error);
    void reg_write_light_mode(std::uint16_t value, Error& error);
    static void reg_write_reset(std::uint16_t value, Error& error);
    void reg_write_serial_baudrate(std::uint16_t value, Error& error);
    void reg_write_serial_address(std::uint16_t value, Error& error);
    void reg_write_serial_parity(std::uint16_t value, Error& error);
    void reg_write_serial_stop_bit(std::uint16_t value, Error& error);
    static void reg_write_rtc_sec(std::uint16_t value, Error& error);
    static void reg_write_rtc_min(std::uint16_t value, Error& error);
    static void reg_write_rtc_hr(std::uint16_t value, Error& error);
    static void reg_write_rtc_w_day(std::uint16_t value, Error& error);
    void reg_write_rtc_calibration(std::uint16_t value, Error& error);

    // Register read
    std::uint16_t reg_read_light_mode();
    static std::uint16_t reg_read_board_version();
    static std::uint16_t reg_read_rtc_second();
    static std::uint16_t reg_read_rtc_minute();
    static std::uint16_t reg_read_rtc_hour();
    static std::uint16_t reg_read_rtc_week_day();

    // Serial init
    void init_baud_rate();
    void init_address();
    void init_parity();
    void init_stop_bit();

    // Variables
    Light_manager*  light_manager;
    littlefs::File* file;
    Serial_config   current_config;

public:
    /* Coils */
    // None

    /* Discrete inputs (status input) */
    // Light status
    static constexpr std::uint16_t discrete_input_light_channel_status_start = 0; // Light channel 1 = this + 1

    /* Input registers */
    // Read holding registers

    /* Holding registers */
    // Versions
    static constexpr std::uint16_t reg_fw_version_address         = 0;
    static constexpr std::uint16_t reg_board_version_address      = 1;

    // Serial address
    static constexpr std::uint16_t reg_serial_address             = 2;

    // Serial baud rate
    static constexpr std::uint16_t reg_serial_baud_rate_address   = 3;

    // Serial parity
    static constexpr std::uint16_t reg_serial_parity_address      = 4;

    // Serial stop bit
    static constexpr std::uint16_t reg_serial_stop_bit_address    = 5;
    static constexpr std::uint8_t  reg_serial_stop_bit_min        = 1;
    static constexpr std::uint8_t  reg_serial_stop_bit_max        = 2;

    // Reset
    static constexpr std::uint16_t reg_reset_address              = 6;
    static constexpr std::uint16_t reg_reset_key                  = 0xD742;

    // Real time clock
    static constexpr std::uint16_t reg_rtc_second                 = 7;
    static constexpr std::uint16_t reg_rtc_minute                 = 8;
    static constexpr std::uint16_t reg_rtc_hour                   = 9;
    static constexpr std::uint16_t reg_rtc_week_day               = 10;
    static constexpr std::uint16_t reg_rtc_calibration            = 11;

    // Light mode
    static constexpr std::uint16_t reg_light_mode_address         = 100;
    static constexpr std::uint16_t reg_light_mode_manual          = 0;
    static constexpr std::uint16_t reg_light_mode_schedule        = 1;

    // Light channel intensity
    static constexpr std::uint16_t reg_light_channel_intensity_address = 1000; // Set ALL light.
                                                                               // Light channel 1 = this + 1
    static constexpr std::uint8_t  reg_light_channel_intensity_max     = 100;
};

#endif //MODBUS_H
