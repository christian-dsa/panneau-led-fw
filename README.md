**Master**  
[![pipeline status](https://gitlab.com/serre1/panneau-led-fw/badges/master/pipeline.svg)](https://gitlab.com/serre1/panneau-led-fw/-/commits/master)
[![coverage report](https://gitlab.com/serre1/panneau-led-fw/badges/master/coverage.svg)](https://gitlab.com/serre1/panneau-led-fw/-/commits/master)

**Develop**  
[![pipeline status](https://gitlab.com/serre1/panneau-led-fw/badges/develop/pipeline.svg)](https://gitlab.com/serre1/panneau-led-fw/-/commits/develop)
[![coverage report](https://gitlab.com/serre1/panneau-led-fw/badges/develop/coverage.svg)](https://gitlab.com/serre1/panneau-led-fw/-/commits/develop)

# Firmware Light Modbus

## Coil Overview
Light channel status: 1 to 5  

## Register Overview
| Description         | Register range |
| :------------------ | :------------- |
| Board configuration | 0 - 9          |
| Light configuration | 100            |
| Light intensity     | 1000 - 1005    |  

## Coil Table
| Coil  | Name                    | Access    | Description                        |
| :---: | :---------------------: | :-------: | :--------------------------------  |
|     1 | Light channel 1 status  | Read Only | 0: Light is off<br> 1: Light is on |
|     2 | Light channel 2 status  | Read Only | 0: Light is off<br> 1: Light is on |
|     3 | Light channel 3 status  | Read Only | 0: Light is off<br> 1: Light is on |
|     4 | Light channel 4 status  | Read Only | 0: Light is off<br> 1: Light is on |
|     5 | Light channel 5 status  | Read Only | 0: Light is off<br> 1: Light is on |


## Register Table
| Register | Name                         | Access       | Limits or Range | Description                                                          |
|  ------: | :-------------------------   | :----------: | :-------------: | :------------------------------------------------------------------  |
|     0    | Firmware version             | Read Only    | 0x100 = v1.0.0  | Firmware version                                                     |
|     1    | Board version                | Read Only    | 0x100 = REV100  | Board version. Read 0 if there is an error.                          |
|     2    | Serial Address               | Read / Write | 1 to 247        | Change to this register are saved but are only in effect when the device is reset. <br>Default to 247 when the current value is out of range. |
|     3    | Serial Baud Rate             | Read / Write | 0 to 9          | 0: 300, 1: 600, 2: 1200, 3: 2400, 4: 4800, 5: 9600, 6: 19200, 7: 38400, 8: 57600, 9:115200. <br>Change to this register are saved but are only in effect when the device is reset. <br>Default to 19200 when the current value is out of range. <br> *REV100 hardware doesn't support 300 & 600 baud. Fallback to 1200.|
|     4    | Serial Parity                | Read / Write | 0 to 3          | 0: None, 1: Odd, 2: Even <br>Change to this register are saved but are only in effect when the device is reset. <br>Default to Even when the current value is out of range.|
|     5    | Serial Stop Bit              | Read / Write | 1 to 2          | 1: One stop bit, 2: Two stop bit <br>Change to this register are saved but are only in effect when the device is reset. <br>Default to one stop bit when the current value is out of range.|
|     6    | Reset                        | Write Only   | 0xD742          | Write 0xD742 to reset the board. Any other value is ignored.         |
|     7    | Real Time Clock, second      | Read / Write | 0 to 59         | Read or write RTC's seconds field. Any other value is ignored. Value is lost when device is powered off |
|     8    | Real Time Clock, minute      | Read / Write | 0 to 59         | Read or write RTC's minutes field. Any other value is ignored. Value is lost when device is powered off |
|     9    | Real Time Clock, hour        | Read / Write | 0 to 24         | Read or write RTC's hours field. Any other value is ignored. Value is lost when device is powered off |
|    10    | Real Time Clock, day of week | Read / Write | 1 to 7          | 1: Monday, 2: Tuesday, 3: Wednesday, 4: Thursday, 5: Friday, 6: Saturday, 7: Sunday. Any other value is ignored. Value is lost when device is powered off |
|    11    | Real Time CLock calibration  | Read / Write | 0 to 65535      | See section on RTC Calibration.                                      |
|   100    | Light mode                   | Read / Write | 0 or 1          | Select operating mode<br> 0: Manual control<br> 1: Follow schedule   |
|  1000    | Light intensity Channel ALL  | Write Only   | 0 to 100        | Intensity in %. In manual mode, set light intensity to all channels. |
|  1001    | Light intensity Channel 1    | Read / Write | 0 to 100        | Intensity in %. In manual mode, set light intensity to channel 1.    |
|  1002    | Light intensity Channel 2    | Read / Write | 0 to 100        | Intensity in %. In manual mode, set light intensity to channel 2.    |
|  1003    | Light intensity Channel 3    | Read / Write | 0 to 100        | Intensity in %. In manual mode, set light intensity to channel 3.    |
|  1004    | Light intensity Channel 4    | Read / Write | 0 to 100        | Intensity in %. In manual mode, set light intensity to channel 4.    |
|  1005    | Light intensity Channel 5    | Read / Write | 0 to 100        | Intensity in %. In manual mode, set light intensity to channel 5.    |

## RTC Calibration

### Register structure
|       | CALP  | Reserved  | CALM  |
| :---: | :---: | :------:  | :---: |
|  Bit  |   15  |    14:9   |  8:0  |

### Measurement steps
1. Set **Real Time CLock calibration** register to 0
2. Measure the calibration clock (1 Hz) on PA4 TP (close to NTC connector)
3. Measure the frequency **for more than 32 seconds**
4. Use the following equation to calculate CALP and CALM. If the measured frequency is too high, CALP = 0

### Equation
CAL_OUT = MEAS_CAL * (1 + (CALP * 512 - CALM) / (2**20 + CALM - CALP * 512))

Python class [Light_modbus](https://gitlab.com/serre1/panneau-led-SW) 
