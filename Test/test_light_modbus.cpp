//
// Created by christian on 2021-03-14.
//
#include "catch2/catch.hpp"
#include "fakeit.hpp"
#include "led.h"
#include "light_manager.h"
#include "modbus.h"
#include "pwm.h"

using namespace fakeit;

SCENARIO("Modbus light control", "[modbus light control]") {

    // BSP mock
    Mock<Pwm> mock_pwm_ch1;
    Mock<Pwm> mock_pwm_ch2;
    Mock<Pwm> mock_pwm_ch3;
    Mock<Pwm> mock_pwm_ch4;
    Mock<Pwm> mock_pwm_ch5;

    // Set PWM period to 100 to facilitate check
    When(Method(mock_pwm_ch1, get_period)).AlwaysReturn(100);
    When(Method(mock_pwm_ch2, get_period)).AlwaysReturn(100);
    When(Method(mock_pwm_ch3, get_period)).AlwaysReturn(100);
    When(Method(mock_pwm_ch4, get_period)).AlwaysReturn(100);
    When(Method(mock_pwm_ch5, get_period)).AlwaysReturn(100);

    Fake(Method(mock_pwm_ch1, open), Method(mock_pwm_ch1, close), Method(mock_pwm_ch1, set_pulse_width));
    Fake(Method(mock_pwm_ch2, open), Method(mock_pwm_ch2, close), Method(mock_pwm_ch2, set_pulse_width));
    Fake(Method(mock_pwm_ch3, open), Method(mock_pwm_ch3, close), Method(mock_pwm_ch3, set_pulse_width));
    Fake(Method(mock_pwm_ch4, open), Method(mock_pwm_ch4, close), Method(mock_pwm_ch4, set_pulse_width));
    Fake(Method(mock_pwm_ch5, open), Method(mock_pwm_ch5, close), Method(mock_pwm_ch5, set_pulse_width));


    Led_dimmable led_ch1(mock_pwm_ch1.get());
    Led_dimmable led_ch2(mock_pwm_ch2.get());
    Led_dimmable led_ch3(mock_pwm_ch3.get());
    Led_dimmable led_ch4(mock_pwm_ch4.get());
    Led_dimmable led_ch5(mock_pwm_ch5.get());

    constexpr std::uint32_t nb_led = 5;
    std::array<light_entry_t , nb_led> led_array = {{
                                                            {led_ch1, 0},
                                                            {led_ch2, 0},
                                                            {led_ch3, 0},
                                                            {led_ch4, 0},
                                                            {led_ch5, 0}
                                                    }};

    Light_list light_list(led_array.data(), led_array.size());
    Light_manager light_manager(light_list);
    Modbus modbus;

    modbus.set_light_manager(light_manager);

    Modbus::Error mb_error;

    GIVEN("Client set light mode") {

        WHEN("client set mode to schedule") {
            modbus.holding_register_write(Modbus::reg_light_mode_address, Modbus::reg_light_mode_schedule, mb_error);

            THEN("mode is set to schedule") {
                REQUIRE( light_manager.get_mode() == Light_manager::Mode::schedule );
                REQUIRE( mb_error == Modbus::Error::ok );
            }
        }

        WHEN("client set mode to manual") {
            modbus.holding_register_write(Modbus::reg_light_mode_address, Modbus::reg_light_mode_manual, mb_error);

            THEN("mode is set to manual") {
                REQUIRE( light_manager.get_mode() == Light_manager::Mode::manual );
                REQUIRE( mb_error == Modbus::Error::ok );
            }
        }
    }

    GIVEN("Light mode is set to schedule") {
        modbus.holding_register_write(Modbus::reg_light_mode_address, Modbus::reg_light_mode_schedule, mb_error);

        WHEN("client read mode with both input register & holding register read") {
            std::uint16_t mode = modbus.holding_register_read(Modbus::reg_light_mode_address, mb_error);
            std::uint16_t mode_ir = modbus.input_register_read(Modbus::reg_light_mode_address, mb_error);

            THEN("mode is schedule") {
                REQUIRE( mode     == Modbus::reg_light_mode_schedule );
                REQUIRE( mode_ir  == Modbus::reg_light_mode_schedule );
                REQUIRE( mb_error == Modbus::Error::ok );
            }
        }
    }


    GIVEN("Client control the light channels manually") {
        light_manager.open_light();
        light_manager.enter_manual_mode();

        Verify(Method(mock_pwm_ch1, open));
        Verify(Method(mock_pwm_ch1, set_pulse_width).Using(0));
        VerifyNoOtherInvocations(mock_pwm_ch1);

        Verify(Method(mock_pwm_ch2, open));
        Verify(Method(mock_pwm_ch2, set_pulse_width).Using(0));
        VerifyNoOtherInvocations(mock_pwm_ch2);

        Verify(Method(mock_pwm_ch3, open));
        Verify(Method(mock_pwm_ch3, set_pulse_width).Using(0));
        VerifyNoOtherInvocations(mock_pwm_ch3);

        Verify(Method(mock_pwm_ch4, open));
        Verify(Method(mock_pwm_ch4, set_pulse_width).Using(0));
        VerifyNoOtherInvocations(mock_pwm_ch4);

        Verify(Method(mock_pwm_ch5, open));
        Verify(Method(mock_pwm_ch5, set_pulse_width).Using(0));
        VerifyNoOtherInvocations(mock_pwm_ch5);

        WHEN("client set ch1 to 50%, ch2 to 25%, ch3 to 75%, ch4 to 10% and ch5 to 100%") {
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 1, 50, mb_error);
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 2, 25, mb_error);
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 3, 75, mb_error);
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 4, 10, mb_error);
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 5, 100, mb_error);

            THEN("light on channel x is set to y%") {
                Verify(Method(mock_pwm_ch1, set_pulse_width).Using(50));
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, close));

                Verify(Method(mock_pwm_ch2, set_pulse_width).Using(25));
                VerifyNoOtherInvocations(Method(mock_pwm_ch2, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch2, close));

                Verify(Method(mock_pwm_ch3, set_pulse_width).Using(75));
                VerifyNoOtherInvocations(Method(mock_pwm_ch3, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch3, close));

                Verify(Method(mock_pwm_ch4, set_pulse_width).Using(10));
                VerifyNoOtherInvocations(Method(mock_pwm_ch4, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch4, close));

                Verify(Method(mock_pwm_ch5, set_pulse_width).Using(101)); // 100% PWM = period + 1
                VerifyNoOtherInvocations(Method(mock_pwm_ch5, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch5, close));
            }
        }

        WHEN("client set ch8 to 50%") {
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 8, 50, mb_error);

            THEN("an error message is sent to client since this value is out of range") {
                REQUIRE(mb_error == Modbus::Error::out_of_range);
            }
        }

        WHEN("client set all channel to 20%") {
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address, 20, mb_error);

            THEN("all light are at 20%") {
                Verify(Method(mock_pwm_ch1, set_pulse_width).Using(20));
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, close));

                Verify(Method(mock_pwm_ch2, set_pulse_width).Using(20));
                VerifyNoOtherInvocations(Method(mock_pwm_ch2, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch2, close));

                Verify(Method(mock_pwm_ch3, set_pulse_width).Using(20));
                VerifyNoOtherInvocations(Method(mock_pwm_ch3, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch3, close));

                Verify(Method(mock_pwm_ch4, set_pulse_width).Using(20));
                VerifyNoOtherInvocations(Method(mock_pwm_ch4, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch4, close));

                Verify(Method(mock_pwm_ch5, set_pulse_width).Using(20));
                VerifyNoOtherInvocations(Method(mock_pwm_ch5, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch5, close));
            }
        }

        WHEN("schedule task run and hit a job") {
            light_manager.schedule_write(Light_channel::ch1, 20);

            THEN("job is ignored") {
                VerifyNoOtherInvocations(mock_pwm_ch1);
            }
        }

        WHEN("client set ch1 to > 100%") {
            // Generate all invalid values
            std::uint16_t intensity_out_of_range_min = 101;
            std::uint16_t intensity_out_of_range_max = 105; // shorter test
//            std::uint16_t intensity_out_of_range_max = std::numeric_limits<std::uint16_t>::max();
            std::uint16_t new_intensity = GENERATE_REF(
                    range<std::uint32_t>(intensity_out_of_range_min, intensity_out_of_range_max + 1));

            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 1, new_intensity, mb_error);

            THEN("an error message is sent to client since this value is out of range. Set light to 100%") {
                REQUIRE(mb_error == Modbus::Error::write_error);
                Verify(Method(mock_pwm_ch1, set_pulse_width).Using(101)); // 101 = 100%
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, close));
            }
        }
    }

    GIVEN("Client control the light channels in schedule mode") {
        modbus.holding_register_write(Modbus::reg_light_mode_address, Modbus::reg_light_mode_schedule, mb_error);

        WHEN("schedule task run and hit a job to set ch1 to 75%") {
            light_manager.schedule_write(Light_channel::ch1, 75);

            THEN("light is set") {
                Verify(Method(mock_pwm_ch1, set_pulse_width).Using(75));
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, set_pulse_width));
                VerifyNoOtherInvocations(Method(mock_pwm_ch1, close));
            }
        }

        WHEN("client set ch1 manually to 50%") {
            modbus.holding_register_write(Modbus::reg_light_channel_intensity_address + 1, 50, mb_error);

            THEN("command is ignored") {
                VerifyNoOtherInvocations(mock_pwm_ch1);
            }
        }

        /// Out of range value
        WHEN("client set mode to invalid value") {
            // Generate all invalid values
            std::uint16_t mode_out_of_range_min = 2;
            std::uint16_t mode_out_of_range_max = 3; // shorter test
//            std::uint16_t mode_out_of_range_max = std::numeric_limits<std::uint16_t>::max();
            std::uint16_t new_mode = GENERATE_REF(range<std::uint32_t>(mode_out_of_range_min, mode_out_of_range_max + 1));

            modbus.holding_register_write(Modbus::reg_light_mode_address, new_mode, mb_error);

            THEN("an error message is sent to client since this value is out of range. Ignore the new value") {
                REQUIRE( mb_error == Modbus::Error::write_error );
                std::uint16_t current_mode = modbus.holding_register_read(Modbus::reg_light_mode_address, mb_error);
                REQUIRE( current_mode == Modbus::reg_light_mode_schedule );
            }
        }
    }

    GIVEN("Light feedback") {

        Light_list::Error error;


        light_list.open_all();
        light_list.on(Light_channel::ch1, 15, error);
        light_list.on(Light_channel::ch2, 25, error);
        light_list.off(Light_channel::ch3,    error);
        light_list.on(Light_channel::ch4, 45, error);
        light_list.on(Light_channel::ch5, 55, error);

        WHEN("client read ch2 (on) light status (On/Off) by reading coil and discrete input") {
            bool ch_status = modbus.discrete_input_read(Modbus::discrete_input_light_channel_status_start + 2, mb_error);
            bool ch_status_coil = modbus.coil_read(Modbus::discrete_input_light_channel_status_start + 2, mb_error);


            THEN("light status is returned") {
                REQUIRE(ch_status      == true );
                REQUIRE(ch_status_coil == true );
            }
        }

        WHEN("client read ch3 (off) light status (On/Off) by reading coil") {
            bool ch_status = modbus.discrete_input_read(Modbus::discrete_input_light_channel_status_start + 3, mb_error);

            THEN("light status is returned") {
                REQUIRE( ch_status == false );
            }
        }

        WHEN("client read all channels light status (On/Off) by reading coil") {
            bool ch_status = modbus.discrete_input_read(Modbus::discrete_input_light_channel_status_start, mb_error);

            THEN("light status is returned") {
                REQUIRE( mb_error == Modbus::Error::out_of_range );
                REQUIRE( ch_status == false );
            }
        }

        WHEN("client read ch8 light status (On/Off) by reading coil") {
            bool ch_status = modbus.discrete_input_read(Modbus::discrete_input_light_channel_status_start + 8, mb_error);

            THEN("out of range error is returned") {
                REQUIRE( mb_error == Modbus::Error::out_of_range );
                REQUIRE( ch_status == false );
            }
        }

        WHEN("client read ch2 light intensity by reading register") {
            std::uint16_t intensity;

            intensity = modbus.holding_register_read(Modbus::reg_light_channel_intensity_address + 2, mb_error);

            THEN("light intensity is returned") {
                REQUIRE( intensity == 25 );
            }
        }

        WHEN("client read ch24 (out of range) light intensity by reading register") {
            std::uint16_t intensity;

            intensity = modbus.holding_register_read(Modbus::reg_light_channel_intensity_address + 24, mb_error);

            THEN("out of range error is returned") {
                REQUIRE( mb_error == Modbus::Error::out_of_range );
                REQUIRE( intensity == 0 );
            }
        }
    }
}
