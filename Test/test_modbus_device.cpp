//
// Created by christian on 2021-03-21.
//

#include "bsp.h"
#include "catch2/catch.hpp"
#include "modbus.h"
#include "test_bsp_mock.h"
#include "version.h"


SCENARIO("Modbus device", "[modbus device]") {

    bsp::Board::init();
    Modbus modbus;

    // This test must be updated for every version
    GIVEN("Firmware version is v0.2.0") {

        WHEN("client read register 'Firmware version'") {
            Modbus::Error mb_error;

            // Using holding register
            uint16_t fw_version_hr = modbus.holding_register_read(Modbus::reg_fw_version_address, mb_error);
            // Using input register
            uint16_t fw_version_ir = modbus.input_register_read(Modbus::reg_fw_version_address, mb_error);

            THEN("client get 0x020") {
                REQUIRE( mb_error == Modbus::Error::ok );
                REQUIRE( fw_version_hr == fw_version );
                REQUIRE( fw_version_ir == fw_version );
            }
        }
    }

    GIVEN("Board version is REV100") {

        WHEN("client read register 'Board version'") {
            Modbus::Error mb_error;

            // Using holding register
            uint16_t board_version_hr = modbus.holding_register_read(Modbus::reg_board_version_address, mb_error);
            // Using input register
            uint16_t board_version_ir = modbus.input_register_read(Modbus::reg_board_version_address, mb_error);

            THEN("it get 0x010") {
                REQUIRE( mb_error == Modbus::Error::ok );
                REQUIRE( board_version_hr == 0x100 );
                REQUIRE( board_version_ir == 0x100 );
            }
        }
    }

    GIVEN("Board is running") {

        WHEN("client reset device via modbus") {
            Modbus::Error mb_error;

            modbus.holding_register_write(Modbus::reg_reset_address, Modbus::reg_reset_key, mb_error);

            THEN("the bsp is called and reset the MCU") {
                REQUIRE( bsp::mock::is_board_reset() == true );
                REQUIRE( mb_error == Modbus::Error::ok );
            }
        }

        WHEN("client reset device via modbus with the wrong value") {
            Modbus::Error mb_error;

            modbus.holding_register_write(Modbus::reg_reset_address, 28, mb_error);

            THEN("the bsp is NOT called, NO reset") {
                REQUIRE( bsp::mock::is_board_reset() == false );
                REQUIRE( mb_error == Modbus::Error::write_error );
            }
        }
    }
}
