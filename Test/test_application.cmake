find_package(Catch2 REQUIRED)

add_executable(test_application "")

target_sources(test_application
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/catch2_main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/support/test_bsp.cpp
    ${CMAKE_CURRENT_LIST_DIR}/support/test_filesystem.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_light_modbus.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_modbus_device.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_modbus_serial_config.cpp
    ${CMAKE_CURRENT_LIST_DIR}/test_modbus_time_config.cpp
    )

target_include_directories(test_application
    PRIVATE
    ${CMAKE_CURRENT_LIST_DIR}/vendor/fakeit/include
    ${CMAKE_CURRENT_LIST_DIR}/vendor/fakeit/config/catch
    ${PROJECT_SOURCE_DIR}/Application
    ${PROJECT_SOURCE_DIR}/BSP/Board
    ${CMAKE_CURRENT_LIST_DIR}/support
    )

target_link_libraries(test_application
    PRIVATE
    light_manager
    modbus
    fw_drivers
    Catch2::Catch2
    )

target_compile_features(test_application PRIVATE cxx_std_17)

# CTest
#include(CTest)
#include(Catch)
#catch_discover_tests(test_application)
