//
// Created by christian on 2021-03-21.
//
#include <limits>
#include "bsp.h"
#include "catch2/catch.hpp"
#include "littlefs.h"
#include "modbus.h"
#include "support/test_filesystem.h"

SCENARIO("Modbus serial config", "[modbus serial config]") {

    littlefs::Block_device& bd = bsp::Board::get_block_device();
    auto fs_config = get_fs_config();
    littlefs::File_system fs(bd, fs_config);
    fs.format();
    fs.mount();

    // File object is also already created
    std::array<std::uint8_t, cache_size> file_cache {};
    littlefs::File file(fs.get_lfs(), &file_cache);

    Modbus modbus;
    Modbus::Error mb_error;

    modbus.set_file(file);

    GIVEN("Board power up for the first time, config files doesn't exists") {

        WHEN("startup read config") {
            Modbus::Serial_config& config = modbus.get_serial_config();

            THEN("get default values (19200 baud, 8E1)") {
                REQUIRE( config.address   == Modbus::Serial_config::default_serial_address );
                REQUIRE( config.baud_rate == Modbus::Serial_config::default_serial_baud_rate );
                REQUIRE( config.stop_bit  == Modbus::Serial_config::default_serial_stop_bit );
                REQUIRE( config.parity    == Modbus::Serial_config::Parity::even );
                REQUIRE( static_cast<std::uint8_t>(config.parity) == 2 ); // enum value fit Micrium DEFINE
                REQUIRE( config.get_baudrate_numeric_value()      == 19200 );
            }
        }
    }

    GIVEN("Board power up for the first time, serial is initialized with default values (19200 baud, 8E1)") {
        modbus.get_serial_config();
        // Init hardware here

        WHEN("user read register Serial Address") {
            std::uint16_t address = modbus.holding_register_read(Modbus::reg_serial_address, mb_error);

            THEN("it get 247") {
                REQUIRE( address  == Modbus::Serial_config::default_serial_address );
                REQUIRE( mb_error == Modbus::Error::ok );
            }
        }
        WHEN("user read register Serial Baud Rate") {
            std::uint16_t baud_rate = modbus.holding_register_read(Modbus::reg_serial_baud_rate_address, mb_error);

            THEN("it get 6 (19200)") {
                REQUIRE( baud_rate == Modbus::Serial_config::Baud_rate::_19200_baud );
                REQUIRE( mb_error  == Modbus::Error::ok );
            }
        }
        WHEN("user read register Serial Parity") {
            std::uint16_t parity = modbus.holding_register_read(Modbus::reg_serial_parity_address, mb_error);

            THEN("it get 2 (EVEN)") {
                REQUIRE( parity   == Modbus::Serial_config::Parity::even );
                REQUIRE( mb_error == Modbus::Error::ok );
            }
        }
        WHEN("client read register Serial Stop Bit") {
            std::uint16_t stop_bit = modbus.holding_register_read(Modbus::reg_serial_stop_bit_address, mb_error);

            THEN("it get 1 (1 stop bit)") {
                REQUIRE( stop_bit == Modbus::Serial_config::default_serial_stop_bit );
                REQUIRE( mb_error == Modbus::Error::ok );
            }
        }

        WHEN("client change the baud rate to 115200") {
            modbus.holding_register_write(Modbus::reg_serial_baud_rate_address, Modbus::Serial_config::_115200_baud, mb_error);
            REQUIRE( mb_error  == Modbus::Error::ok );

            THEN("the value read when reading register Serial Baud Rate is still 19200 (6)") {
                std::uint16_t baud_rate = modbus.holding_register_read(Modbus::reg_serial_baud_rate_address, mb_error);
                REQUIRE( baud_rate == Modbus::Serial_config::_19200_baud );
            }
        }
        WHEN("client change the parity to ODD") {
            modbus.holding_register_write(Modbus::reg_serial_parity_address, Modbus::Serial_config::Parity::odd, mb_error);
            REQUIRE( mb_error  == Modbus::Error::ok );

            THEN("the value read when reading register Serial Parity is still EVEN (2)") {
                std::uint16_t parity = modbus.holding_register_read(Modbus::reg_serial_parity_address, mb_error);
                REQUIRE( parity == Modbus::Serial_config::Parity::even );
            }
        }
        WHEN("client change the address to 24") {
            modbus.holding_register_write(Modbus::reg_serial_address, 24, mb_error);
            REQUIRE( mb_error == Modbus::Error::ok );

            THEN("the value read when reading register Serial Address is still 247") {
                std::uint16_t address = modbus.holding_register_read(Modbus::reg_serial_address, mb_error);
                REQUIRE( address  == Modbus::Serial_config::default_serial_address );
            }
        }
        WHEN("client change the stop bit to 2") {
            modbus.holding_register_write(Modbus::reg_serial_stop_bit_address, 2, mb_error);
            REQUIRE( mb_error == Modbus::Error::ok );

            THEN("the value read when reading register Serial Stop bit is still 1") {
                std::uint16_t stop_bit = modbus.holding_register_read(Modbus::reg_serial_stop_bit_address, mb_error);
                REQUIRE( stop_bit  == Modbus::Serial_config::default_serial_stop_bit );
            }
        }
    }

    GIVEN("Application is running with default values") {
        Modbus::Serial_config& config = modbus.get_serial_config();

        /// Baud rate
        WHEN("client change baud rate with a valid value and reset the board") {
            // Generate all valid values
            std::uint16_t baud_rate_min = Modbus::Serial_config::Baud_rate::_300_baud;
            std::uint16_t baud_rate_max = Modbus::Serial_config::Baud_rate::_115200_baud;
            std::uint16_t new_baud_rate = GENERATE_REF(range<std::uint16_t>(baud_rate_min, baud_rate_max + 1));

            modbus.holding_register_write(Modbus::reg_serial_baud_rate_address, new_baud_rate, mb_error);
            modbus.holding_register_write(Modbus::reg_reset_address, Modbus::reg_reset_key, mb_error);
            REQUIRE(mb_error == Modbus::Error::ok);

            THEN("board reset, initialize serial with new baud rate. Register Serial Baud Rate now read entered value") {
                static constexpr std::array<std::uint32_t, 10> baudrate_lt = {300, 600, 1200, 2400, 4800,
                                                                              9600, 19200, 38400, 57600, 115200};
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE(new_config.baud_rate == new_baud_rate);
                REQUIRE(new_config.get_baudrate_numeric_value() ==
                        baudrate_lt[new_baud_rate]); // value given to serial peripherals init

                std::uint16_t baud_rate = new_modbus.holding_register_read(Modbus::reg_serial_baud_rate_address,
                                                                           mb_error);
                REQUIRE(baud_rate == new_baud_rate);
            }
        }

        /// Serial address
        WHEN("client change Serial Address to 15 and reset the board") {
            constexpr std::uint8_t new_address = 15;

            modbus.holding_register_write(Modbus::reg_serial_address, new_address, mb_error);
            modbus.holding_register_write(Modbus::reg_reset_address, Modbus::reg_reset_key, mb_error);
            REQUIRE(mb_error == Modbus::Error::ok);

            THEN("board reset, initialize serial with new address. Register Serial Address now read 15") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE(new_config.address == new_address);

                std::uint16_t address = new_modbus.holding_register_read(Modbus::reg_serial_address, mb_error);
                REQUIRE(address == new_address);
            }
        }

        /// Parity
        WHEN("client change Serial Parity with a valid value and reset the board") {
            // Generate all valid values
            std::uint16_t parity_min = Modbus::Serial_config::Parity::none;
            std::uint16_t parity_max = Modbus::Serial_config::Parity::even;
            std::uint16_t new_parity = GENERATE_REF(range<std::uint16_t>(parity_min, parity_max + 1));

            modbus.holding_register_write(Modbus::reg_serial_parity_address, new_parity, mb_error);
            modbus.holding_register_write(Modbus::reg_reset_address, Modbus::reg_reset_key, mb_error);
            REQUIRE(mb_error == Modbus::Error::ok);

            THEN("board reset, initialize serial with new parity. Register Serial parity now read entered value") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE(new_config.parity == new_parity);

                std::uint16_t parity = new_modbus.holding_register_read(Modbus::reg_serial_parity_address, mb_error);
                REQUIRE(parity == new_parity);
            }
        }

        /// Stop bit
        WHEN("client change Serial Stop Bit with a valid value and reset the board") {
            // Generate all valid values
            std::uint16_t stop_bit_min = 1;
            std::uint16_t stop_bit_max = 2;
            std::uint16_t new_stop_bit = GENERATE_REF(range<std::uint16_t>(stop_bit_min, stop_bit_max + 1));

            modbus.holding_register_write(Modbus::reg_serial_stop_bit_address, new_stop_bit, mb_error);
            modbus.holding_register_write(Modbus::reg_reset_address, Modbus::reg_reset_key, mb_error);
            REQUIRE(mb_error == Modbus::Error::ok);

            THEN("board reset, initialize serial stop bit. Register Serial stop bit now read entered value") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE(new_config.stop_bit == new_stop_bit);

                std::uint16_t stop_bit = new_modbus.holding_register_read(Modbus::reg_serial_stop_bit_address,
                                                                          mb_error);
                REQUIRE(stop_bit == new_stop_bit);
            }
        }
    }

    GIVEN("Application is running with 115200 8N1, address 64") {
        constexpr std::uint8_t address = 64;
        constexpr std::uint8_t stop_bit = 1;

        modbus.holding_register_write(Modbus::reg_serial_address, address, mb_error);
        modbus.holding_register_write(Modbus::reg_serial_baud_rate_address, Modbus::Serial_config::Baud_rate::_115200_baud, mb_error);
        modbus.holding_register_write(Modbus::reg_serial_parity_address, Modbus::Serial_config::Parity::none, mb_error);
        modbus.holding_register_write(Modbus::reg_serial_stop_bit_address, stop_bit, mb_error);

        modbus.holding_register_write(Modbus::reg_reset_address, Modbus::reg_reset_key, mb_error);
        modbus.get_serial_config();

        /// Out of range test
        WHEN("client change baud rate register to any value between 10 and 65535 and reset the board") {
            // Generate all invalid values
            constexpr std::uint16_t baud_out_of_range = 10;
            // constexpr std::uint16_t baud_register_max = std::numeric_limits<std::uint16_t>::max(); // Full test
            constexpr std::uint16_t baud_register_max = 10; // for shorter tests
            auto new_baud_rate = GENERATE_REF(range<std::uint32_t>(baud_out_of_range, baud_register_max + 1));

            modbus.holding_register_write(Modbus::reg_serial_baud_rate_address, new_baud_rate, mb_error);
            REQUIRE( mb_error == Modbus::Error::write_error );

            modbus.holding_register_write(Modbus::reg_reset_address, Modbus::reg_reset_key, mb_error);

            THEN("new baud rate is ignored, board reset") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE( new_config.baud_rate == Modbus::Serial_config::Baud_rate::_115200_baud );
                REQUIRE( new_config.get_baudrate_numeric_value() == 115200 ); // value given to serial peripherals init

                std::uint16_t baud_rate = new_modbus.holding_register_read(Modbus::reg_serial_baud_rate_address, mb_error);
                REQUIRE( baud_rate == Modbus::Serial_config::Baud_rate::_115200_baud );
            }
        }

        WHEN("client change address register to any value between 248 and 255 and reset the board") {
            constexpr std::uint8_t address_out_of_range = 248;
            constexpr std::uint8_t address_register_max = std::numeric_limits<std::uint8_t>::max();

            // Generate address 248 to 255 inclusive. Range doesn't include last value, thus address_register_max + 1.
            auto new_address = GENERATE_REF(range<std::uint16_t>(address_out_of_range, address_register_max + 1));

            modbus.holding_register_write(Modbus::reg_serial_address, new_address, mb_error);
            REQUIRE( mb_error == Modbus::Error::write_error );

            THEN("new value is ignored, board reset") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE( new_config.address == address );
                REQUIRE( modbus.holding_register_read(Modbus::reg_serial_address, mb_error) == address );
            }
        }

        WHEN("client change address register to 0 (broadcast) and reset the board") {
            auto new_address = 0;

            modbus.holding_register_write(Modbus::reg_serial_address, new_address, mb_error);
            REQUIRE( mb_error == Modbus::Error::write_error );

            THEN("new value is ignored, board reset") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE( new_config.address == address );
                REQUIRE( modbus.holding_register_read(Modbus::reg_serial_address, mb_error) == address );
            }
        }

        WHEN("client change parity register to any value between 3 and 65535 and reset the board") {
            constexpr std::uint16_t parity_out_of_range = 248;
//            constexpr std::uint16_t parity_register_max = std::numeric_limits<std::uint16_t>::max();
            constexpr std::uint16_t parity_register_max = 249; // for shorter tests

            // Generate address 3 to 65535 inclusive. Range doesn't include last value, thus parity_register_max + 1.
            auto new_parity = GENERATE_REF(range<std::uint32_t>(parity_out_of_range, parity_register_max + 1));

            modbus.holding_register_write(Modbus::reg_serial_parity_address, new_parity, mb_error);
            REQUIRE( mb_error == Modbus::Error::write_error );

            THEN("new value is ignored, board reset") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE( new_config.parity == Modbus::Serial_config::Parity::none );
                REQUIRE( modbus.holding_register_read(Modbus::reg_serial_parity_address, mb_error) == Modbus::Serial_config::Parity::none );
            }
        }

        WHEN("client change stop bit register to any value between 3 and 65535 and reset the board") {
            constexpr std::uint16_t stop_out_of_range = 3;
//            constexpr std::uint16_t stop_register_max = std::numeric_limits<std::uint16_t>::max();
            constexpr std::uint16_t stop_register_max = 4; // for shorter tests

            // Generate address 3 to 65535 inclusive. Range doesn't include last value, thus parity_register_max + 1.
            auto new_stop_bit = GENERATE_REF(range<std::uint32_t>(stop_out_of_range, stop_register_max + 1));

            modbus.holding_register_write(Modbus::reg_serial_stop_bit_address, new_stop_bit, mb_error);
            REQUIRE( mb_error == Modbus::Error::write_error );

            THEN("new value is ignored, board reset") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE( new_config.stop_bit == stop_bit );
                REQUIRE( modbus.holding_register_read(Modbus::reg_serial_stop_bit_address, mb_error) == stop_bit );
            }
        }
        WHEN("client change stop bit register to 0 and reset the board") {
            auto new_stop_bit = 0;

            modbus.holding_register_write(Modbus::reg_serial_stop_bit_address, new_stop_bit, mb_error);
            REQUIRE( mb_error == Modbus::Error::write_error );

            THEN("new value is ignored, board reset") {
                fs.unmount();
                fs.mount();
                Modbus new_modbus;         // New instance since old one is lost in reset
                new_modbus.set_file(file); // Board reset complete

                Modbus::Serial_config& new_config = new_modbus.get_serial_config();
                REQUIRE( new_config.stop_bit == stop_bit );
                REQUIRE( modbus.holding_register_read(Modbus::reg_serial_stop_bit_address, mb_error) == stop_bit );
            }
        }
    }

    fs.unmount();
}
