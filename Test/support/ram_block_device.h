//
// Created by christian on 2021-05-09.
//

#ifndef RAM_BLOCK_DEVICE_H
#define RAM_BLOCK_DEVICE_H

#include "littlefs.h"

template<std::uint8_t nb_page, std::uint16_t page_size> // page_size in byte
class Ram_io : public littlefs::Block_device {
public:
    Ram_io()
    {
        std::fill(&ram_memory[0][0], &ram_memory[nb_page - 1][page_size - 1], 0xFF);
    }

    int read(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, void *buffer, lfs_size_t size) override
    {
        auto read_iterator = (std::uint64_t*)buffer;

        for (std::uint32_t i = 0; i < size; i += sizeof(std::uint64_t)) {
            *read_iterator = *(std::uint64_t*)(&ram_memory[block][off + i]);
            ++read_iterator;
        }
        return 0;
    }

    int prog(const struct lfs_config *c, lfs_block_t block, lfs_off_t off, const void *buffer, lfs_size_t size) override
    {
        auto write_iterator = (const std::uint64_t*)buffer;

        for (std::uint32_t i = 0; i < size; i += sizeof(std::uint64_t)) {
            *(std::uint64_t*)(&ram_memory[block][off + i]) = *write_iterator;
            ++write_iterator;
        }
        return 0;
    }

    int erase(const struct lfs_config *c, lfs_block_t block) override
    {
        std::fill(&ram_memory[block][0], &ram_memory[block][page_size - 1], 0xFFFFFFFF);
        return 0;
    }

    int sync(const struct lfs_config *c) override
    {
        return 0;
    }

    void reset()
    {
        std::fill(&ram_memory[0][0], &ram_memory[nb_page - 1][page_size - 1], 0xFFFFFFFFFFFFFFFF);
    }

    uint8_t ram_memory[nb_page][page_size] {};
};


#endif //RAM_BLOCK_DEVICE_H
