//
// Created by christian on 2021-09-09.
//

#ifndef TEST_BSP_MOCK_H
#define TEST_BSP_MOCK_H

namespace bsp::mock {

    bool is_board_reset();
}

#endif //TEST_BSP_MOCK_H
