//
// Created by christian on 2021-09-09.
//

#include <array>
#include "test_filesystem.h"

// File system array
std::array<std::uint8_t, cache_size> fs_prog {};
std::array<std::uint8_t, cache_size> fs_read {};
std::array<std::uint8_t, lookahead_size> fs_lookahead {};

littlefs::fs_config_t fs_config = {
        .read_size        = sizeof(std::uint64_t),
        .prog_size        = sizeof(std::uint64_t),
        .block_size       = page_size,
        .block_count      = np_page,
        .block_cycles     = 500,
        .name_max         = 0,
        .file_max         = 0,
        .attr_max         = 0,
        .metadata_max     = 0,
        .read_buffer      = fs_read.data(),
        .prog_buffer      = fs_prog.data(),
        .cache_size       = cache_size,
        .lookahead_buffer = fs_lookahead.data(),
        .lookahead_size   = lookahead_size
};

littlefs::fs_config_t& get_fs_config()
{
    return fs_config;
}
