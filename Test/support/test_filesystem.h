//
// Created by christian on 2021-09-09.
//

#ifndef TEST_FILESYSTEM_H
#define TEST_FILESYSTEM_H

#include "littlefs.h"

constexpr std::uint8_t  np_page = 2;
constexpr std::uint16_t page_size = 2048;
constexpr std::uint32_t cache_size = 16;
constexpr std::uint32_t lookahead_size = 16;

littlefs::fs_config_t& get_fs_config();

#endif //TEST_FILESYSTEM_H
