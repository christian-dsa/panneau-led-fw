//
// Created by christian on 2021-09-09.
//

#include "bsp.h"
#include "ram_block_device.h"
#include "test_bsp_mock.h"
#include "test_filesystem.h"

using namespace bsp;

// Board reset
static bool is_reset = false;

// Block device
static Ram_io<np_page, page_size> ram_io; // NOLINT(cert-err58-cpp)

// RTC
static std::uint16_t rtc_calibration_value;

class Dut_time : public util::Time {
public:
    void update_time() override
    {
        // Take RTC's value and update time_t
    }

    void update_date() override
    {
        // Take RTC's value and update date_t
    }

    void update_calendar() override
    {
        // Take RTC's values, update time_t and date_t
    }

    void set_time(const util::time::time_t& time_) override
    {
        calendar.time = time_;
    }

    void set_date(const util::time::date_t& date_) override
    {
        calendar.date = date_;
    }

    void set_calendar(const util::time::calendar_t& calendar_) override
    {
        calendar.time = calendar_.time;
        calendar.date = calendar_.date;
    }
};

static Dut_time dut_time; // NOLINT(cert-err58-cpp)

void Board::init()
{
    is_reset = false;
    rtc_calibration_value = 0;

    util::time::calendar_t time_at_boot {.date{0}, .time{0}};
    dut_time.set_calendar(time_at_boot);
}

Board::Version Board::get_board_version() {
    return Board::Version::rev100;
}

void Board::reset()
{
    is_reset = true;
}

bool mock::is_board_reset()
{
    return is_reset;
}

littlefs::Block_device& Board::get_block_device()
{
    ram_io.reset();
    return ram_io;
}

void Board::set_rtc_calibration(std::uint16_t calibration)
{
    rtc_calibration_value = calibration;
}

std::uint16_t Board::get_rtc_calibration()
{
    return rtc_calibration_value;
}

util::Time& Board::get_time_instance()
{
    return dut_time;
}
