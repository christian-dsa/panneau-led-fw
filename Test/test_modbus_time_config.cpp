//
// Created by christian on 2021-09-09.
//
#include "bsp.h"
#include "catch2/catch.hpp"
#include "files.h"
#include "littlefs.h"
#include "modbus.h"
#include "support/test_filesystem.h"


SCENARIO("Modbus time config", "[modbus time config]") {
    bsp::Board::init();

    littlefs::Block_device& bd = bsp::Board::get_block_device();
    auto fs_config = get_fs_config();
    littlefs::File_system fs(bd, fs_config);
    fs.format();
    fs.mount();

    // File object is also already created
    std::array<std::uint8_t, cache_size> file_cache{};
    littlefs::File file(fs.get_lfs(), &file_cache);

    Modbus modbus;
    Modbus::Error mb_error;

    modbus.set_file(file);

    GIVEN("Board power up for the first time") {
        // Application read file system. It creates the calibration file and set it to 0.
        constexpr std::uint16_t default_calibration_value = 0;
        file.open(file_rtc_calib, "w");
        file.write(default_calibration_value);
        file.close();

        bsp::Board::set_rtc_calibration(default_calibration_value);

        WHEN("RTC calibration is read over modbus") {
            std::uint16_t calibration_value = modbus.holding_register_read(Modbus::reg_rtc_calibration, mb_error);

            THEN("calibration is disabled") {
                constexpr std::uint16_t disabled = 0;
                REQUIRE( mb_error == Modbus::Error::ok );
                REQUIRE( calibration_value == disabled );
            }
        }

        WHEN("RTC calibration is set to 33085 via modbus") {
            modbus.holding_register_write(Modbus::reg_rtc_calibration, 33085, mb_error);

            THEN("board's RTC calibration value is set to 33085") {
                REQUIRE( mb_error == Modbus::Error::ok );
                REQUIRE( bsp::Board::get_rtc_calibration() == 33085 );
            }
            THEN("board's RTC calibration value is saved in filesystem") {
                std::uint16_t calibration_value;
                file.open(file_rtc_calib, "r");
                file.read(calibration_value);
                file.close();

                REQUIRE( calibration_value == 33085 );
            }
        }

        WHEN("device time's is set to Friday 23:59:59") {
            constexpr std::uint16_t sec = 59;
            constexpr std::uint16_t min = 59;
            constexpr std::uint16_t hr = 23;
            constexpr std::uint16_t w_day = util::time::week_day::friday;

            modbus.holding_register_write(Modbus::reg_rtc_second, sec, mb_error);
            modbus.holding_register_write(Modbus::reg_rtc_minute, min, mb_error);
            modbus.holding_register_write(Modbus::reg_rtc_hour, hr, mb_error);
            modbus.holding_register_write(Modbus::reg_rtc_week_day, w_day, mb_error);

            THEN("RTC's time is set to Friday 23:59:59") {
                constexpr util::time::time_t expected_t {.hour = hr, .min = min, .sec = sec};
                constexpr util::time::date_t expected_d {.week_day = w_day };

                util::time::calendar_t current_time = bsp::Board::get_time_instance().get_calendar();

                REQUIRE( current_time.date.week_day == expected_d.week_day );
                REQUIRE( current_time.time.sec  == expected_t.sec );
                REQUIRE( current_time.time.min  == expected_t.min );
                REQUIRE( current_time.time.hour == expected_t.hour );
            }
        }
    }

    GIVEN("Board's RTC value as been previously configured with 97") {
        // Application read file system. It reads the calibration file and set the RTC.
        constexpr std::uint16_t calibration_value_on_boot = 97;
        file.open(file_rtc_calib, "w");
        file.write(calibration_value_on_boot);
        file.close();

        bsp::Board::set_rtc_calibration(calibration_value_on_boot);

        WHEN("RTC calibration is read") {
            std::uint16_t calibration_value = modbus.holding_register_read(Modbus::reg_rtc_calibration, mb_error);

            THEN("calibration value is 97") {
                REQUIRE( mb_error == Modbus::Error::ok );
                REQUIRE( calibration_value == 97 );
            }
        }

        WHEN("RTC calibration is changed to 201") {
            modbus.holding_register_write(Modbus::reg_rtc_calibration, 201, mb_error);

            THEN("calibration is set to 201") {
                REQUIRE( mb_error == Modbus::Error::ok );
                REQUIRE( bsp::Board::get_rtc_calibration() == 201 );
            }
            THEN("board's RTC calibration value is saved in filesystem") {
                std::uint16_t calibration_value;
                file.open(file_rtc_calib, "r");
                file.read(calibration_value);
                file.close();

                REQUIRE( calibration_value == 201 );
            }
        }
    }

    GIVEN("Current time is Monday 23:12:54") {
        constexpr util::time::calendar_t now {.date{.week_day=util::time::week_day::monday},
                                                 .time{.hour=23, .min=12, .sec=54}};

        bsp::Board::get_time_instance().set_calendar(now);

        WHEN("set seconds to impossible value") {
            constexpr std::uint16_t sec = 66;

            modbus.holding_register_write(Modbus::reg_rtc_second, sec, mb_error);

            THEN("value is ignored") {
                util::time::calendar_t current_time = bsp::Board::get_time_instance().get_calendar();

                REQUIRE( current_time.time.sec  == now.time.sec );
                REQUIRE( current_time.time.min  == now.time.min );
                REQUIRE( current_time.time.hour == now.time.hour );
                REQUIRE( current_time.date.week_day == now.date.week_day );
                REQUIRE( mb_error == Modbus::Error::write_error );
            }
        }

        WHEN("set min to impossible value") {
            constexpr std::uint16_t min = 60;

            modbus.holding_register_write(Modbus::reg_rtc_minute, min, mb_error);

            THEN("value is ignored") {
                util::time::calendar_t current_time = bsp::Board::get_time_instance().get_calendar();

                REQUIRE( current_time.time.sec  == now.time.sec );
                REQUIRE( current_time.time.min  == now.time.min );
                REQUIRE( current_time.time.hour == now.time.hour );
                REQUIRE( current_time.date.week_day == now.date.week_day );
                REQUIRE( mb_error == Modbus::Error::write_error );
            }
        }

        WHEN("set hour to impossible value") {
            constexpr std::uint16_t hr = 30;

            modbus.holding_register_write(Modbus::reg_rtc_hour, hr, mb_error);

            THEN("value is ignored") {
                util::time::calendar_t current_time = bsp::Board::get_time_instance().get_calendar();

                REQUIRE( current_time.time.sec  == now.time.sec );
                REQUIRE( current_time.time.min  == now.time.min );
                REQUIRE( current_time.time.hour == now.time.hour );
                REQUIRE( current_time.date.week_day == now.date.week_day );
                REQUIRE( mb_error == Modbus::Error::write_error );
            }
        }

        WHEN("set week day to impossible value") {
            constexpr std::uint16_t w_day = 8;

            modbus.holding_register_write(Modbus::reg_rtc_week_day, w_day, mb_error);

            THEN("value is ignored") {
                util::time::calendar_t current_time = bsp::Board::get_time_instance().get_calendar();

                REQUIRE( current_time.time.sec  == now.time.sec );
                REQUIRE( current_time.time.min  == now.time.min );
                REQUIRE( current_time.time.hour == now.time.hour );
                REQUIRE( current_time.date.week_day == now.date.week_day );
                REQUIRE( mb_error == Modbus::Error::write_error );
            }
        }
    }

    GIVEN("Device time is Monday 12:10:09") {
        constexpr util::time::calendar_t device_time {.date {.week_day = util::time::week_day::monday},
                                                      .time{.hour = 12, .min = 10, .sec = 9}
                                                     };

        bsp::Board::get_time_instance().set_calendar(device_time);

        WHEN("client read time") {
            util::time::calendar_t read_time {};

            read_time.time.sec = modbus.holding_register_read(Modbus::reg_rtc_second, mb_error);
            read_time.time.min = modbus.holding_register_read(Modbus::reg_rtc_minute, mb_error);
            read_time.time.hour = modbus.holding_register_read(Modbus::reg_rtc_hour, mb_error);
            read_time.date.week_day = modbus.holding_register_read(Modbus::reg_rtc_week_day, mb_error);

            THEN("it gets Monday 12:10:09") {
                REQUIRE( read_time.time.sec      == device_time.time.sec );
                REQUIRE( read_time.time.min      == device_time.time.min );
                REQUIRE( read_time.time.hour     == device_time.time.hour );
                REQUIRE( read_time.date.week_day == device_time.date.week_day );
            }
        }
    }
}
